FROM node:11

RUN mkdir /theme
WORKDIR /theme
COPY ["theme/package.json", "theme/package-lock.json", "/theme/"]
RUN rm -rf node_modules
RUN npm install
COPY ["theme/gulpfile.js", "/theme/"]
COPY ["theme/src", "/theme/src/"]
RUN npm run build


FROM python:3.6

ARG HOTPOTATO_BUILD_DEV

ENV HOTPOTATO_LOG_DIR="${HOTPOTATO_LOG_DIR:-/var/log/hotpotato}"
ENV FLASK_APP="hotpotato.app.main:app"

ENV FLASK_DEBUG="${FLASK_DEBUG:-false}"

RUN mkdir -p /code
RUN mkdir -p "${HOTPOTATO_LOG_DIR}"

COPY ["docker/install", "/install"]
COPY ["docker/start", "/start"]

RUN chmod +x /install /start

COPY [".", "/code"]
COPY --from=0 ["/theme/dist", "/code/theme/dist"]

RUN /install

ENTRYPOINT ["dumb-init", "/start"]

EXPOSE 8000
