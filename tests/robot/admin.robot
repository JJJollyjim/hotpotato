*** Settings ***
Library           SeleniumLibrary
Documentation     Validate the admin page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Test Teardown     Logout


*** Test Cases ***
Open Admin Page
    Login
    Go To Admin Page
    Admin Page Should Be Open

Add New User
    Login
    Go To Admin Page
    Open Add User Dialog
    Add User Dialog Should Be Open
    Input Users Name     ${NAME}
    Input Email         ${EMAIL}
    Submit Add User Modal
    ${password} =   Shows Password
    Close Add User Dialog
    Add User Dialog Should Not Be Open
    Logout
    Login With Credentials    username=${EMAIL}   password=${password}

Add New Team
    Login
    Go To Admin Page
    Open Add Team Dialog
    Add Team Dialog Should Be Open
    Input Team Name
    Submit Add Team Modal
    Team Page Should Be Open


*** Variables ***
${NAME}     Test
${EMAIL}    test@example.com


*** Keywords ***
Go To Admin Page
    Click Element   id: account-dropdown
    Click Link      Admin

Admin Page Should Be Open
    Page Should Contain     Admin
    Page Should Contain Button  //button[normalize-space(text())='Add team']
    Page Should Contain Button  //button[normalize-space(text())='Add user']

Open Add User Dialog
    Click Button    //button[normalize-space(text())='Add user']

Add User Dialog Should Be Open
    Page Should Contain Textfield   id:add_user-name
    Page Should Contain Textfield   id:add_user-email

Input Users Name
    [Arguments]     ${username}
    Wait Until Element Is Visible   id:add_user-name
    Input Text      id:add_user-name    ${username}

Input Email
    [Arguments]     ${email}
    Input Text      id:add_user-email   ${email}

Submit Add User Modal
    Click Button    //*[@id="new-user-form"]//button[normalize-space(text())='Add user']

Shows Password
    Wait Until Element Is Visible     id:new-user-password
    ${password} =   Get Text    id:new-user-password
    [Return]    ${password}

Close Add User Dialog
    Click Button    //*[@id="add-user"]//*[contains(@class, "modal-stage-2")]//button[normalize-space(text())='Close']

Add User Dialog Should Not Be Open
    Element Should Not Be Visible   id:add_user-name
    Element Should Not Be Visible   id:add_user-email
    Wait Until Element Is Visible   id:account-dropdown

Open Add Team Dialog
    Click Button    //button[normalize-space(text())='Add team']

Add Team Dialog Should Be Open
    Wait Until Element Is Visible   name

Input Team Name
    Input Text      name     'Test team name'

Submit Add Team Modal
    Click Button    //*[@id="add-team"]//button[normalize-space(text())='Add team']

Team Page Should Be Open
    Page Should Contain     Users
    Page Should Contain     Roles
    Page Should Contain Button     //button[normalize-space(text())='Add existing users']
    Page Should Contain Button     //button[normalize-space(text())='Remove team']
