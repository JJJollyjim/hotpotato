*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${BROWSER}        Firefox

${LOGIN URL}      http://hotpotato:8000
${LOGOUT URL}      http://hotpotato:8000/logout
${SERVERS URL}      http://hotpotato:8000/servers
${NOTIFICATIONS URL}      http://hotpotato:8000/notifications
${HANDOVER URL}      http://hotpotato:8000/handover
${MESSAGE URL}      http://hotpotato:8000/message
${ACCOUNT URL}      http://hotpotato:8000/account
${ADMIN URL}        http://hotpotato:8000/admin

${USERNAME test1}     test1@example.com
${PASSWORD test1}     test_password1

${USERNAME test2}     test2@example.com
${PASSWORD test2}     test_password2


*** Keywords ***
#
# Login/logout keywords.
#

Open Browser To Login Page
    Set Selenium Implicit Wait    1 second
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Page Should Contain    Login

Login Page Should Be Open
    Page Should Contain    Login

Input Username
    [Arguments]    ${username}
    Input Text    email    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    password    ${password}

Input Credentials For test1
    Input Username    ${USERNAME test1}
    Input Password    ${PASSWORD test1}

Input Credentials For test2
    Input Username    ${USERNAME test2}
    Input Password    ${PASSWORD test2}

Input Valid Credentials
    Input Credentials For test1

Input Invalid Credentials
    Input Username  blah@blah.blah
    Input Password  blahblahblah

Submit Credentials
    Click Button    submit

Logout
    Go To   ${LOGOUT URL}

Login
    Login to test1

Login With Credentials
    [Arguments]     ${username}=    ${password}=
    Input Username  ${username}
    Input password  ${password}
    Submit Credentials
    Page Should Not Contain    Login

Login to test1
    Input Credentials For test1
    Submit Credentials
    Page Should Not Contain    Login

Login to test2
    Input Credentials For test2
    Submit Credentials
    Page Should Not Contain    Login

#
# Page (webpage) keywords.
#

Page Should Show Pager Name
    Page Should Contain    Notifications
    Page Should Contain    Take Tater

#
# Home page keywords.
#

Go To Home Page
    Click Link    class: navbar-brand

Home Page Should Be Open
    Page Should Contain     the tater
    Page Should Contain     Take Tater
    Page Should Contain     Notifications

#
# Server keywords.
#

Go To Servers Page
    Click Link    Servers

Servers Page Should Be Open
    Page Should Contain    Add server
    Page Should Contain    All servers 

#
# Notifications keywords.
#

Go To Notifications Page
    Click Link    Notifications

Notifications Page Should Be Open
    Page Should Contain    Export

#
# Handover keywords.
#

Open Handover Dialog
    Click Button   Take Tater

Handover Dialog Should Be Open
    Page Should Contain    So, you ain't no spec-tater...

#
# Send Message keywords.
#

Open Message Dialog
    Click Button   id: message-tater-button

Send Message Dialog Should Be Open
    Page Should Contain    Message the tater holder

#
# Account settings keywords.
#

Go To Account Settings Page
    Click Element       id: account-dropdown
    Click Link    Account Settings

#
# Change Password keywords.
#

Go To Change Password Page
    Click Element       id: account-dropdown
    Click Link    Change Password
