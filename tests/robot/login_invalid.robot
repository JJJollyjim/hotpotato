*** Settings ***
Documentation     Validate flask-security's behaviour with invalid logins.
Test Template     Login with invalid credentials should fail
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser
Resource          resources/common.robot


*** Test Cases ***    USERNAME             PASSWORD
Invalid Username      invalid@blah.blah    ${PASSWORD test1}
Invalid Password      ${USERNAME test1}    invalid
Invalid Both          invalid@blah.blah    invalid

*** Keywords ***
Login with invalid credentials should fail
    [Arguments]    ${username}    ${password}
    Input Username    ${username}
    Input Password    ${password}
    Submit Credentials
    Login Should Have Failed
    Should Not Be Able To Access Secure Pages

Should Not Be Able To Access Secure Pages
    Should Not Be Able To Access    ${LOGIN URL}
    Should Not Be Able To Access    ${SERVERS URL}
    Should Not Be Able To Access    ${NOTIFICATIONS URL}
    Should Not Be Able To Access    ${ACCOUNT URL}
    Should Not Be Able To Access    ${ADMIN URL}

Login Should Have Failed
    Page Should Contain    Incorrect username or password

Should Not Be Able To Access
    [Arguments]    ${url}
    Go To    ${url}
    Login Page Should Be Open
