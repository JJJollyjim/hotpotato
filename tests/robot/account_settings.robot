*** Settings ***
Documentation     Validate the Account Settings page.
Resource          resources/common.robot
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser


*** Test Cases ***
Open Account Settings Page
    Login
    Go To Account Settings Page
    Account Settings Page Should Be Open

Add Contact Method
    Click Add Contact Method
    Select SMS Contact Method
    Add A SMS Number
    Add A Contact Priority
    Submit Add A SMS Number
    Account Settings Page Should Be Open
    Account Settings Page Should Contain SMS Number

Add Duplicate Contact Method
    Click Add Contact Method
    Select SMS Contact Method
    Add A SMS Number
    Add A Contact Priority
    Submit Add A SMS Number
    Contact Should Already Exist
    Close Add Account Settings Form

Delete Contact Method
    Go To Account Settings Page
    Account Settings Page Should Be Open
    Select Delete Checkbox
    Submit Delete SMS Number
    Account Settings Page Should Be Open
    Number Should Be Deleted
    Logout

*** Keywords ***
Account Settings Page Should Be Open
    Page Should Contain    My contact methods

Account Settings Page Should Contain SMS Number
    Page Should Contain    +6464588885555

Click Add Contact Method
    Click Link    Add contact method

Contact Should Already Exist
    Page Should Contain    Contact method already exists

Select SMS Contact Method
    Select From List By Value    method    sms

Add A SMS Number
#    Select From List By Label    sms
    Input Text    contact    +6464588885555

Add A Contact Priority
    Input Text    priority    0

Submit Add A SMS Number
    Click Button    Add

Select Delete Checkbox
    Select Checkbox    +6464588885555

Submit Delete SMS Number
    Click Button    Delete

Number Should Be Deleted
    Page Should Not Contain    +6464588885555

Close Add Account Settings Form
    Click Button    Close
