# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [v0.9.1] - 2019-03-01

### Added
- Added a pipenv command to automatically generate migrations from [@callum027](https://gitlab.com/Callum027)

### Changed
- The padding on the notification page has been reduced and colours changed to make it easier to read from [@rhysmd](https://gitlab.com/rhysmd)

### Fixed
- Fixed the copy notification link/details buttons from [@zacps](https://gitlab.com/zacps)
- Improved the queries and rendering for the notification page to make it faster from [@zacps](https://gitlab.com/zacps)

### Security
- Fixed insecure use of make_response allowing reflected XSS on 404 pages fix from [@zacps](https://gitlab.com/zacps) reported by Sam Banks


## [v0.9.0] - 2019-02-19

### Added
- Automated migration testing from [@rhysmd](https://gitlab.com/rhysmd)
- Debug sender in testing environment from [@zacps](https://gitlab.com/zacps)
- New CI coverage report from [@zacps](https://gitlab.com/zacps)
- New API tests from [@rhysmd](https://gitlab.com/rhysmd)
- Add a database creation cli command and stamp database with alembic revision on creation from [@rhysmd](https://gitlab.com/rhysmd)
- Add more context to custom message notifications from [@zacps](https://gitlab.com/zacps)
- Add support for teams from [@zacps](https://gitlab.com/zacps)
- Add standard & emergency priority pushover alerts from [@callum027](https://gitlab.com/Callum027)

### Changed
- Replace ModelObject with SQLAlchemy single table inheritance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor Docker set-up to simplify maintenance from [@rhysmd](https://gitlab.com/rhysmd)
- Re-factor API notification handling from [@callum027](https://gitlab.com/Callum027)
- Remove rabbitmq and cockroach from the docker container from [@rhysmd](https://gitlab.com/rhysmd)
- Redesign forms and servers page from [@rhysmd](https://gitlab.com/rhysmd)
- Change all POST endpoints to require a CSRF token [@rhysmd](https://gitlab.com/rhysmd)

### Fixed
- Fix up issues in test data generation from [@callum027](https://gitlab.com/Callum027)
- Fix output of Modica notification status function from [@rhysmd](https://gitlab.com/rhysmd)
- Fix errors display when no user is currently on pager from [@rhysmd](https://gitlab.com/rhysmd)
- Fix creating a user with the active flag from [@callum027](https://gitlab.com/Callum027)
- Fix loading configuration from a file from [@callum027](https://gitlab.com/Callum027)
