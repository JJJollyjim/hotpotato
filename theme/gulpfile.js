const gulp = require('gulp');
const gulpif = require('gulp-if');
const gulpcache = require('gulp-cache');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const flexfixes = require('postcss-flexbugs-fixes');
const cssnano = require('cssnano');
const concatjs = require('gulp-concat');
const uglifyjs = require('gulp-uglify');
const babel = require('gulp-babel');


const PATHS = {
  'src': {
    'root': './src/**',
    'js': './src/js/**/*.js',
    'scss': './src/scss/**/*.scss'
  },
  'dist': {
    'root': './dist/',
    'js': './dist/js/',
    'css': './dist/css/'
  }
}

gulp.task('copy-files', () => {
  return gulp.src([PATHS.src.root, '!' + PATHS.src.scss, '!' + PATHS.src.js])
    .pipe(gulp.dest(PATHS.dist.root));
});

gulp.task('scss', () => {
  return gulp.src(PATHS.src.scss)
    .pipe(sourcemaps.init())
    .pipe(sass({
        includePaths: [
          './node_modules/'
          ]
      })
      .on('error', sass.logError)
    )
    .pipe(postcss([ // building, run minification
        autoprefixer({
          browsers: ['last 2 versions'],
          cascade: false,
          remove: false
        }),
        flexfixes(),
        cssnano()
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(PATHS.dist.css))
});

gulp.task('js', function() {
  return gulp.src([
      './node_modules/jquery/dist/jquery.js',
      './node_modules/popper.js/dist/umd/popper.js',
      './node_modules/bootstrap/dist/js/bootstrap.js',
      './node_modules/animejs/anime.min.js',
      './node_modules/clipboard/dist/clipboard.js',
      './node_modules/selectize/dist/js/standalone/selectize.js',
      PATHS.src.js
    ])
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: [
        ['@babel/env'],
        {
          // This tells babel to try interpret whether an import is a module
          // or a script based on imports/exports. If this gets it wrong
          // explicitly setting `sourceType` might be a better option.
          // https://stackoverflow.com/questions/34973442/how-to-stop-babel-from-transpiling-this-to-undefined-and-inserting-use-str
          sourceType: "unambiguous"
        }
      ]
    }))
    .pipe(concatjs('build.js'))
    .pipe(uglifyjs({ mangle: false }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(PATHS.dist.js));
});

gulp.task('cache-clear', function(done) {
  gulpcache.clearAll();
  done();
});

gulp.task('build', gulp.parallel('copy-files', 'scss', 'js'));

gulp.task('watch', () => {
  gulp.watch(PATHS.src.root, gulp.series('cache-clear', 'build'));
});

gulp.task('default', gulp.series('copy-files', 'scss', 'js', 'watch'));
