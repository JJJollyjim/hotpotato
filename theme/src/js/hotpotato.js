// Initialise BS Tooltip
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(() => {
  $('[data-toggle="tooltip"]').tooltip({
    animation: false
  });
});

// Adding/removing focus state class for BS preppend/append inputs (global)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  $('.form-control').focus(function () {
    $(this)
      .prev('.input-group-prepend')
      .addClass('input-group-prepend-focus');
    $(this)
      .next('.input-group-append')
      .addClass('input-group-append-focus');
  });
  $('.form-control').focusout(function () {
    // Removing focus class
    $(this)
      .prev('.input-group-prepend-focus')
      .removeClass('input-group-prepend-focus');
    $(this)
      .next('.input-group-append-focus')
      .removeClass('input-group-append-focus');
  });
});
// Servers notifications enable/disable toggler
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(() => {
  $('.alert-checkbox').change(function () {
    $(this)
      .closest('.list-group-item')
      .toggleClass('disabled');
  });
});

// 'Tater button' animation (via anime.js)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(() => {
  const pathBtn = 'M 62.05 56 C 43.967 56 25.883 56 7.8 56 C 3.5 56 0 52.5 0 48.3 C 0 37.433 0 26.567 0 15.7 C 0 11.5 3.5 8 7.8 8 C 16.4 8 25 8 33.6 8 C 42.2 8 50.8 8 59.4 8 C 78.333 8 97.267 8 116.2 8 C 116.2 8 116.2 8 116.2 8 C 120.5 8 124 11.5 124.1 15.7 C 124.1 26.533 124.1 37.367 124.1 48.2 C 124.1 52.5 120.6 56 116.3 56 C 107.258 56 98.217 56 89.175 56 L 62.05 56';
  const pathTater = 'M 63.7 68 C 57.3 68.1 50.9 67.4 44.7 66 C 30.9 62.7 18.3 54.1 9.4 42.8 C 4.6 36.7 -0.5 29 0 20.8 C 0.5 12.4 7.1 6.6 14.3 3.9 C 22.2 1 29.2 2 37.1 3.6 C 47.5 5.7 52.8 7.2 62.8 3.7 C 73.3 0.05 83.075 -0.475 90.975 0.338 C 98.875 1.15 104.9 3.3 107.9 5 C 114.2 8.6 119.7 14 122.4 20.9 C 124.7 26.75 124.45 32.4 122.5 37.5 C 120.55 42.6 116.9 47.15 112.4 50.8 C 98.6 61.9 81.2 67.8 63.7 68 L 63.7 68';

  const fillBtn = '#F5512A';
  const fillTater = '#895063';

  function taterAnim(el, path, fill) {
    // Declare anime.js animated properties
    anime.remove(el);
    anime({
      targets: el,
      d: path,
      easing: 'easeOutElastic', // easeOutElastic
      duration: 1000,
      fill
    });
  }

  function transformToTater(el) {
    // Define svg paths for each end of anim
    taterAnim(el, pathTater, fillTater);
  }

  function transformToBtn(el) {
    taterAnim(el, pathBtn, fillBtn);
  }

  $('.btn-tater').each(function () {
    // Find tater - bind events
    $(this).on('mouseover', function () {
      transformToTater(
        $(this)
          .find('#btn-tater')
          .get(0)
      );
    });
    $(this).on('mouseout', function () {
      transformToBtn(
        $(this)
          .find('#btn-tater')
          .get(0)
      );
    });
  });
});

// Auto focus modal inputs
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

$(document).ready(() => {
  $('.modal').on('shown.bs.modal', () => {
    $('.input-focused').trigger('focus');
  });
});

// Team switcher
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  $('.team-switcher').on('change', function () {
    this.form.submit();
  });
});

// Escalates-to switcher
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  $('#escalates-to').on('change', function () {
    this.form.submit();
  });
});

function addTwoStepForm(selector, fillDataCallback, formCloseCallback) {
  $(`${selector} form`).on('submit', (event) => {
    event.preventDefault();

    const data = new FormData(event.target);

    fetch(event.target.action, {
      method: 'POST',
      body: data
    }).then((res) => {
      if (res.ok) {
        res.json().then((respData) => {
          $(`${selector} .modal-stage-2 .modal-body.success`).show();
          fillDataCallback(respData);

          // Show second modal stage
          $(`${selector} .modal-stage-1`).removeClass('show');
          $(`${selector} .modal-stage-1`).hide();
          $(`${selector} .modal-stage-1`).addClass('d-none');
          $(`${selector} .modal-stage-2`).removeClass('d-none');
          $(`${selector} .modal-stage-2`).addClass('show');

          // Prepare for use again
          $(this).on('hidden.bs.modal', () => {
            $(`${selector} .modal-stage-1`).removeClass('d-none');
            $(`${selector} .modal-stage-1`).addClass('show');
            $(`${selector} .modal-stage-1`).show();
            $(`${selector} .modal-stage-2`).removeClass('show');
            $(`${selector} .modal-stage-2`).addClass('d-none');
            event.target.reset();
            formCloseCallback(respData);
          });
        });
      } else {
        res.json().then((errors) => {
          $(`${selector} form .form-control`).removeClass('is-invalid');
          $(`${selector} form .invalid-feedback`).remove();
          Object.keys(errors).forEach((field) => {
            $(`#${field}`).addClass('is-invalid');
            $(`#${field}`).after(`<div class="invalid-feedback">${errors[field]}</div>`);
          });
        });
      }
    });
  });
}

// Two-step form
$(document).ready(() => {
  addTwoStepForm('#add-user', (resp) => {
    document.getElementById('new-user-password').innerHTML = resp.password;
  });

  addTwoStepForm(
    '#addServerModal',
    (resp) => {
      document.getElementById('server-api-key').innerHTML = resp.apikey;
    },
    () => {
      window.location.reload();
    }
  );
});

// Add user to team combo box
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  const selectize = $('#users').selectize({
    preload: 'focus',
    valueField: 'id',
    labelField: 'name',
    sortField: 'last_login_at',
    searchField: 'name',
    plugins: ['no_results'],
    load(query, callback) {
      fetch(
        `/api/web/v1/users/get?query=${encodeURIComponent(query)}&__team_id=${encodeURIComponent(
          $('#team-id').text()
        )}`,
        {
          credentials: 'same-origin'
        }
      )
        .then((response) => {
          switch (response.status) {
            case 200:
              response.json().then((json) => {
                if (!json.length) {
                  selectize[0].selectize.displayEmptyResultsMessage();
                }
                callback(json);
              });
              break;
            default:
              throw new Error(response.status);
          }
        })
        .catch((error) => {
          // TODO: Actual error handling
          console.error(error);
        });
    }
  });
});

// Expand textarea to match it's initial height.
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  document.querySelectorAll('textarea.expanded').forEach((area) => {
    const styles = window.getComputedStyle(area);
    const pt = parseInt(styles.getPropertyValue('padding-top'), 10);
    const pb = parseInt(styles.getPropertyValue('padding-bottom'), 10);
    elem.style.height = `${elem.scrollHeight + pt + pb}px`;
  });
});

// Auto open modals
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  $('[data-display-open]').removeClass('fade');
  $('[data-display-open]').modal('show');
  $('[data-display-open]').addClass('fade');
});


// Copy to clipboard buttons
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
$(document).ready(() => {
  /* eslint-disable no-new */
  new ClipboardJS('[data-clipboard-text]');
});
