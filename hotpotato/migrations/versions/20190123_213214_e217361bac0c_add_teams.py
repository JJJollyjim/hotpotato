"""
Add teams

Revision ID: e217361bac0c
Revises: d86032547328
Create Date: 2019-01-23 21:32:14.748552

This does not attempt to do a perfect migration.
In particular, the following is excluded:

Upgrade:
* Transfering on-call from the sysadmin rotation to the hotpotato team.

Downgrade:
* Removing roles from other teams. If roles with the same name are added to
  multiple teams this will cause the downgrade to fail.
"""


import sqlalchemy as sa
from alembic import context, op

# revision identifiers, used by Alembic.
revision = "e217361bac0c"
down_revision = "3023312d9843"
branch_labels = None
depends_on = None

num_insert_ops = 1


def database_insert(table, data, row=None):
    """
    Helper function for adding data to a table.

    Give it the table and a mutable data list, and it will insert rows in a controlled
    manner, by splitting the insert operation into smaller ones with a maximum 100 rows.

    Insert data into it by passing it to the row keyword argument, as a dictionary.

    Once all the data has been run through this function, call it one more time
    without the row parameter to flush the rest of tha data.
    """

    global num_insert_ops

    if not row or len(data) >= 100:
        print("{} INSERT {}".format(num_insert_ops, len(data)))
        op.bulk_insert(table, data)
        data.clear()
        num_insert_ops += 1

    if row:
        data.append(row)


def row_select(session, table, *columns, **search_params):
    """
    Return the results of a SELECT query.

    Returns a generator of single elements if there was only one column specified,
    otherwise returns a generator of arrays of elements.
    """

    cols = [table.columns[col] for col in columns]

    query = session.query(*cols)

    if search_params:
        for key, value in search_params.items():
            query = query.filter(table.columns[key] == value)

    return (data[0] for data in query) if len(columns) == 1 else query


def row_update(table, row_id, **columns):
    """
    Update a row on the given table with the given column values.
    """

    context.execute(table.update().where(table.c.id == row_id).values(**columns))


def upgrade():
    """
    Upgrade the database.
    """

    print("Performing upgrade migration...")

    with context.begin_transaction():
        schema_data_upgrade()

    print("Upgrade migration complete.")


def downgrade():
    """
    Downgrade the database to the previous version.
    """

    print("Performing downgrade migration...")

    data_downgrade()
    with context.begin_transaction():
        schema_downgrade()

    print("Downgrade migration complete.")


def schema_data_upgrade():
    """
    Upgrade the database schema and data.

    Because we're adding NOT NULL relationships data migrations have to happen
    part way through the schema changes.
    """

    print("Performing schema upgrade...")

    # Create new tables
    op.create_table(
        "team",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("escalates_to_id", sa.Integer(), nullable=True),
        sa.Column("on_call_id", sa.Integer(), nullable=True),
        sa.CheckConstraint("id != escalates_to_id", name="not_escalates_to_self"),
        sa.ForeignKeyConstraint(["escalates_to_id"], ["team.id"], ondelete="RESTRICT"),
        sa.ForeignKeyConstraint(["on_call_id"], ["user.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
    )
    op.create_table(
        "teams_users",
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("team_id", sa.Integer(), nullable=False),
        sa.Column("primary", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(["team_id"], ["team.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("user_id", "team_id"),
    )

    # --- BEGIN DATA ----
    # Create the default hotpotato team so we can dump all existing roles/notifications/etc under it
    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)
    team = sa.Table(
        "team",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("escalates_to_id", sa.Integer(), nullable=True),
        sa.Column("on_call_id", sa.Integer(), nullable=True),
        sa.CheckConstraint("id != escalates_to_id", name="not_escalates_to_self"),
        sa.ForeignKeyConstraint(["escalates_to_id"], ["team.id"], ondelete="SET NULL"),
        sa.ForeignKeyConstraint(["on_call_id"], ["user.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
    )

    rotations_table = sa.Table(
        "rotations",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("rotname", sa.Text(), nullable=True),
        sa.Column("person", sa.Integer(), nullable=True),
    )

    on_call_id = next(
        row_select(session, rotations_table, "person", rotname="sysadmins")
    )
    hotpotato_team_id = next(
        session.execute(
            team.insert()
            .values(name="Hot Potato", on_call_id=on_call_id)
            .returning(team.c.id)
        )
    )[0]

    # Migrate users to the new team
    users_table = sa.Table(
        "user",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.Text),
    )
    teams_users = sa.Table(
        "teams_users",
        sa.MetaData(bind=bind),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("team_id", sa.Integer(), nullable=False),
        sa.Column("primary", sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(["team_id"], ["team.id"], ondelete="CASCADE"),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"], ondelete="CASCADE"),
        sa.PrimaryKeyConstraint("user_id", "team_id"),
    )
    users = row_select(session, users_table, "id")
    op.bulk_insert(
        teams_users,
        [
            {"user_id": user_id, "team_id": hotpotato_team_id, "primary": True}
            for user_id in users
        ],
    )
    session.commit()
    # --- END DATA ---

    # Drop old tables
    op.drop_table("rotations")

    # Add notification_log.team_id
    op.add_column("notification_log", sa.Column("team_id", sa.Integer(), nullable=True))
    op.create_index(
        "notification_log_auto_index_fk_team_id_ref_team",
        "notification_log",
        ["team_id"],
        unique=False,
    )

    # --- BEGIN DATA ---
    # Migrate notifications to the Hotpotato team
    notif_types = frozenset(("alert", "handover", "message"))

    notification_log = sa.Table(
        "notification_log",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.Integer, sa.ForeignKey("User.id", ondelete="SET NULL")),
        sa.Column("received_dt", sa.DateTime, nullable=False),
        sa.Column("notif_type", sa.Enum(*notif_types), nullable=False),
        sa.Column("body", sa.Text, nullable=False),
        sa.Column(
            "json", sa.ext.mutable.MutableDict.as_mutable(sa.JSON), nullable=False
        ),
        sa.Column("team_id", sa.Integer, sa.ForeignKey("Team.id", ondelete="RESTRICT")),
    )

    # Update may be too large to do it sanely because psycopg2's buffer isn't
    # big enough.
    count = session.execute(notification_log.count()).scalar()
    for n in range(0, count, 100):
        session.execute(
            notification_log.update()
            .values({"team_id": hotpotato_team_id})
            .where(
                notification_log.c.id.in_(
                    session.query(notification_log.c.id)
                    .order_by("id")
                    .limit(100)
                    .offset(n)
                )
            )
        )
        session.commit()
    # --- END DATA ---

    op.create_foreign_key(
        None, "notification_log", "team", ["team_id"], ["id"], ondelete="RESTRICT"
    )
    op.drop_column("notification_log", "tenant_id")

    # We need to add this collumn with a default because it is not nullable, after that we drop the default.
    op.add_column(
        "role",
        sa.Column(
            "team_id",
            sa.Integer(),
            nullable=False,
            server_default=sa.sql.expression.text(str(hotpotato_team_id)),
        ),
    )
    op.alter_column("role", "team_id", server_default=None)

    # Dropping a unique index requires cascade.
    context.execute("DROP INDEX role@role_name_key CASCADE;")

    op.create_unique_constraint(None, "role", ["name", "team_id"])
    op.create_index(
        "role_auto_index_fk_team_id_ref_team", "role", ["team_id"], unique=False
    )
    op.create_foreign_key(None, "role", "team", ["team_id"], ["id"], ondelete="CASCADE")
    # ### end Alembic commands ###

    print("Schema upgrade complete.")


def schema_downgrade():
    """
    Downgrade the database schema to the previous version.
    """

    print("Performing schema downgrade...")

    # NOTE: Add print statements for each statement.
    # print("Dropping column 'column_name'...")
    # ### commands auto generated by Alembic - please adjust! ###

    op.create_table(
        "rotations",
        sa.Column(
            "id", sa.INTEGER(), server_default=sa.text("unique_rowid()"), nullable=False
        ),
        sa.Column("rotname", sa.VARCHAR(), nullable=True),
        sa.Column("person", sa.INTEGER(), nullable=True),
        sa.ForeignKeyConstraint(["person"], ["user.id"], name="fk_person_ref_user"),
        sa.PrimaryKeyConstraint("id", name="primary"),
    )

    # Recreate default 'rotation' and transfer on-call member from Hotpotato
    # team if possible
    bind = op.get_bind()
    session = sa.orm.Session(bind=bind)

    rotations = sa.Table(
        "rotations",
        sa.MetaData(bind=bind),
        sa.Column(
            "id", sa.INTEGER(), server_default=sa.text("unique_rowid()"), nullable=False
        ),
        sa.Column("rotname", sa.VARCHAR(), nullable=True),
        sa.Column("person", sa.INTEGER(), nullable=True),
        sa.ForeignKeyConstraint(["person"], ["user.id"], name="fk_person_ref_user"),
        sa.PrimaryKeyConstraint("id", name="primary"),
    )
    teams = sa.Table(
        "team",
        sa.MetaData(bind=bind),
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=False),
        sa.Column("escalates_to_id", sa.Integer(), nullable=True),
        sa.Column("on_call_id", sa.Integer(), nullable=True),
        sa.CheckConstraint("id != escalates_to_id", name="not_escalates_to_self"),
        sa.ForeignKeyConstraint(["escalates_to_id"], ["team.id"], ondelete="SET NULL"),
        sa.ForeignKeyConstraint(["on_call_id"], ["user.id"]),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("name"),
    )
    on_call_id = next(row_select(session, teams, "on_call_id", name="Hot Potato"))
    session.execute(rotations.insert().values(rotname="sysadmins", person=on_call_id))
    session.commit()

    op.drop_constraint("fk_team_id_ref_team", "role", type_="foreignkey")
    # Dropping a unique index requires cascade.
    context.execute("DROP INDEX role_name_team_id_key CASCADE;")

    op.create_unique_constraint("role_name_key", "role", ["name"])
    op.drop_column("role", "team_id")

    op.add_column(
        "notification_log", sa.Column("tenant_id", sa.INTEGER(), nullable=True)
    )
    op.drop_constraint("fk_team_id_ref_team", "notification_log", type_="foreignkey")
    op.drop_column("notification_log", "team_id")
    op.drop_table("teams_users")
    op.drop_table("team")
    # ### end Alembic commands ###

    print("Schema downgrade complete.")


def data_downgrade():
    """
    Downgrade the database data to the previous version.
    """

    pass
