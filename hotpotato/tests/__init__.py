"""
Hot Potato testing module.
"""


import faker
import sqlalchemy.exc

from hotpotato.models import db
from hotpotato.tests import (
    heartbeats,
    notifications,
    oncall_contacts,
    roles,
    server_uptimes,
    servers,
    teams,
    users,
)


def create_data(num_servers=3, num=100, seed=None):
    """
    Creates some test data for testing purposes.
    """

    fake = faker.Faker()
    if seed:
        fake.seed(seed)

    # Teams
    print("Creating teams... ", end="", flush=True)
    for _ in range(num // 10):
        team = teams.create_fake(fake)
        if team:
            try:
                db.session.add(team)
                db.session.commit()
            except (sqlalchemy.exc.InvalidRequestError, sqlalchemy.exc.IntegrityError):
                db.session.rollback()
                continue
            db.session.refresh(team)
            db.session.add_all(roles.create_defaults_for_team(team))
            db.session.commit()
    print("done")

    # Users
    print("Creating users... ", end="", flush=True)
    for _ in range(num):
        user = users.create_fake(fake, db.session)
        if user:
            try:
                db.session.add(user)
                db.session.commit()
            except (sqlalchemy.exc.InvalidRequestError, sqlalchemy.exc.IntegrityError):
                continue
    print("done")

    # On-call contacts
    print("Creating on-call contacts... ", end="", flush=True)
    for _ in range(num):
        onc = oncall_contacts.create_fake(fake)
        if onc:
            db.session.add(onc)
    db.session.commit()
    print("done")

    # Servers
    print("Creating servers, server uptimes and heartbeats... ", end="", flush=True)
    for _ in range(num_servers):
        server = servers.create_fake(fake)
        if not server:
            continue
        db.session.add(server)
        db.session.commit()
        db.session.refresh(server)
        if server:
            db.session.add(server_uptimes.create_fake(fake, server))
            db.session.bulk_save_objects(heartbeats.create_fakes(fake, server))
            db.session.commit()
    print("done")

    # Notifications
    print("Creating notifications... ", end="", flush=True)
    db.session.bulk_save_objects(
        [notifications.create_fake(fake, db.session) for _ in range(num * 10)]
    )
    db.session.commit()
    print("done")
