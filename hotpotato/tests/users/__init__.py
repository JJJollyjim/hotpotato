"""
User unit test functions.
"""


from datetime import date
from random import random

import factory
import flask_security.utils
from factory import post_generation

from hotpotato import models
from hotpotato.models import Team, TeamsUsers, db
from hotpotato.tests import teams, util


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.User
        sqlalchemy_session = db.session
        exclude = "unhashed_passwd"

    email = factory.Faker("email")
    unhashed_passwd = "password"
    password = factory.LazyAttribute(
        lambda obj: flask_security.utils.hash_password(obj.unhashed_passwd)
    )

    name = factory.Faker("name")
    active = True
    confirmed_at = factory.Faker("past_datetime", start_date="-1y")
    last_login_at = factory.Faker("past_datetime", start_date="-364d")

    # Users must have a primary team, this is used to generate one
    @post_generation
    def gen_primary_team(obj, create, extracted, **kwargs):  # noqa: N805
        team = teams.TeamFactory()
        obj.teams_users.append(TeamsUsers(team=team, primary=True))

    @factory.lazy_attribute
    def current_login_at(self):
        last_login_at_days_from_today = (
            date.today().timetuple().tm_yday - self.last_login_at.timetuple().tm_yday
        ) % 365
        return factory.Faker(
            "past_datetime",
            start_date="-{}d".format(365 - last_login_at_days_from_today),
        ).generate({})

    login_count = factory.Faker("random_int", max=20)
    last_login_ip = factory.Faker("ipv4")
    current_login_ip = factory.LazyAttribute(
        lambda obj: obj.last_login_ip
        if random() < 0.5
        else factory.Faker("ipv4").generate({})
    )
    timezone = factory.Faker("timezone")


def create_fake(fake, session):
    """
    Generate a fake user, with randomly set data.
    """

    today = date.today()

    confirmed_at = fake.past_datetime(start_date="-1y")

    last_login_at = fake.past_datetime(start_date="-364d")
    last_login_at_days_from_today = (
        today.timetuple().tm_yday - last_login_at.timetuple().tm_yday
    ) % 365

    # Get a random datetime on a day AFTER last_login_at.
    current_login_at = fake.past_datetime(
        start_date="-{}d".format(365 - last_login_at_days_from_today)
    )

    last_login_ip = fake.ipv4()

    # 50/50 chance for the same or different IP.
    current_login_ip = last_login_ip if fake.boolean() else fake.ipv4()

    return models.User(
        email=fake.email(),
        password=flask_security.utils.hash_password(fake.password()),
        name=fake.name(),
        active=fake.boolean(),
        confirmed_at=confirmed_at,
        last_login_at=last_login_at,
        current_login_at=current_login_at,
        last_login_ip=last_login_ip,
        current_login_ip=current_login_ip,
        login_count=fake.random.randrange(20),
        timezone=fake.timezone(),
        teams_users=[
            TeamsUsers(team=util.get_random_object(session, Team), primary=True)
        ],
    )
