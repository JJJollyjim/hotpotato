from hotpotato.models import TeamsUsers
from hotpotato.tests import teams, users
from hotpotato.users import set_primary_team


def test_set_primary_team(session):
    """
    Check set_primary_team sets the correct team and that there is only one
    primary team set.
    """

    user = users.UserFactory()
    assert user.primary_team is not None
    team = teams.TeamFactory()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()

    set_primary_team(user, team.id)

    teams_users = TeamsUsers.query.filter_by(user_id=user.id, primary=True).all()
    assert len(teams_users) == 1
    assert teams_users[0].team_id == team.id
