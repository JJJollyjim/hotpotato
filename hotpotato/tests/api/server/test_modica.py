"""
Test the modica callback endpoints.
"""

from http import HTTPStatus

from hotpotato.notifications import notifications
from hotpotato.tests import notifications as notifications_test, oncall_contacts, users


def test_modica_incoming_message_no_body(client, session):
    """
    Test that a incoming message with no body returns 422.
    """
    r = client.post("/api/server/v2/senders/modica/mo-message")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_modica_incoming_message_invalid_id(client, session):
    """
    Test that an incoming message response to a mesage id that doesn't exist retuns 400.
    """
    r = client.post(
        "/api/server/v2/senders/modica/mo-message",
        json={
            "id": 7,
            "source": "+6464588885555",
            "destination": "202",
            "content": "Test Message",
            "operator": "Test Operator",
            "reply-to": "5",
        },
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_modica_incoming_message_success(client, session):
    """
    Test that a correctly formatted incoming message returns 204.
    """
    users.UserFactory()
    contact = oncall_contacts.OncallContactsFactory(
        contact="+646458888555", method="sms"
    )
    session.commit()
    alert = notifications_test.NotificationFactory(notif_type="alert")
    alert.json["provider"] = "modica"
    alert.json["provider_notif_id"] = "5"
    session.add(alert)
    session.commit()

    r = client.post(
        "/api/server/v2/senders/modica/mo-message",
        json={
            "id": 7,
            "source": contact.contact,
            "destination": "202",
            "content": "Test Message",
            "operator": "Test Operator",
            "reply-to": "5",
        },
    )

    next(notifications.get_by(message="Test Message"))

    assert r.status_code == HTTPStatus.NO_CONTENT


# TODO This should throw 400 since there is on valid JSON
def test_modica_notification_status_no_body(client, session):
    """
    Test that an empty body on incoming notification status returns 422.
    """
    r = client.post("/api/server/v2/senders/modica/dlr-status")
    assert r.status_code == HTTPStatus.UNPROCESSABLE_ENTITY


def test_modica_notification_status_invalid_id(client, session):
    """
    Test that notification status for an id that does not exist returns 400.
    """
    r = client.post(
        "/api/server/v2/senders/modica/dlr-status",
        json={"message_id": "5", "status": "sent"},
    )
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_modica_notification_status_success(client, session):
    """
    Test that a correctly formatted notification status update returns 204.
    """
    users.UserFactory()
    session.commit()
    alert = notifications_test.NotificationFactory(notif_type="alert")
    alert.json["provider"] = "modica"
    alert.json["provider_notif_id"] = "5"
    session.add(alert)
    session.commit()

    r = client.post(
        "/api/server/v2/senders/modica/dlr-status",
        json={"message_id": "5", "status": "sent"},
    )
    assert r.status_code == HTTPStatus.NO_CONTENT
