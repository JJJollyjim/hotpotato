"""
Test the V1 add notification endpoint
"""

from http import HTTPStatus

from hotpotato.notifications import alerts
from hotpotato.tests import notifications, servers, users


def test_add_notification_empty_request(client, session):
    """
    Test that you get a 400 if you don't put anything in the request.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post("/api/server/v1/add/notification", data=dict(apikey=server.apikey))

    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_add_notification_success(client, session):
    """
    Test that if you submit a well formed request the notification is created and the request
    returns 201.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey=server.apikey,
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22 +0000",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_no_timezone(client, session):
    """
    Test that when you submit a notification without timezone info in the timestamp, the
    servers timezone will be fetched and the time will be processed as if it's in that timezone.
    """
    server = servers.ServerFactory(timezone="Pacific/Auckland")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey=server.apikey,
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-21 22:18:22"


def test_add_notification_iso8601_timestamp(client, session):
    """
    Test that when you submit a notification with an iso8601 timestamp it is created correctly.
    """
    server = servers.ServerFactory(timezone="Pacific/Auckland")
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey=server.apikey,
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22T11:18:22+0000",
            servicename="nginx",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_add_notification_no_servicename(client, session):
    """
    Test that if you leave out the servicename a host type notification is
    created and it returns 201.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post(
        "/api/server/v1/add/notification",
        data=dict(
            apikey=server.apikey,
            troublecode="EXAMPLE123",
            hostname="criticalinfra.example.com",
            displayname="Critical Infrastructure",
            icingadatetime="2018-11-22 11:18:22 +0000",
            state="Anything right?",
            output="CRITICAL everything is broken",
        ),
    )

    assert r.status_code == HTTPStatus.CREATED

    alert = alerts.get(int(r.data))

    assert alert.json["alert_type"] == "host"
    assert alert.json["output"] == "CRITICAL everything is broken"
    assert alert.json["timestamp"] == "2018-11-22 11:18:22"


def test_check_notification_invalid_id(client, session):
    """
    Test that when you check the status of a notification that doesn't exist
    you get a 404.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification", data=dict(apikey=server.apikey, id=4)
    )

    assert r.status_code == HTTPStatus.NOT_FOUND


def test_check_notification_no_id(client, session):
    """
    Test that if you don't include an id you get a 400.
    """
    server = servers.ServerFactory()
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification", data=dict(apikey=server.apikey)
    )

    print(r.status_code)
    assert r.status_code == HTTPStatus.BAD_REQUEST


def test_check_notification_success(client, session):
    """
    Tests that if you check for an alert that exists you get a 200.
    """
    server = servers.ServerFactory()
    users.UserFactory()
    alert = notifications.NotificationFactory(notif_type="alert")
    session.commit()

    r = client.post(
        "/api/server/v1/check/notification",
        data=dict(apikey=server.apikey, id=alert.id),
    )

    assert r.status_code == HTTPStatus.OK
    assert r.get_data(as_text=True) == "true"
