import csv
from datetime import datetime
from http import HTTPStatus

import pytest

from hotpotato import teams
from hotpotato.models import TeamsUsers
from hotpotato.tests import notifications, users
from hotpotato.tests.conftest import login


def test_get_notifications_no_notifications(app, client, session):
    """
    Test that a request for a period with no notifications returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27&start_date=2018-11-26&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True


def test_get_notifications_one_notification(app, client, session):
    """
    Test that a request for a period with one notification returns that one notification.
    """
    user = users.UserFactory(timezone="UTC")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 25, hour=11), team=team
        )
        session.commit()
        print(alert.received_dt)
        r = client.get(
            "/api/web/v1/notifications/get?end_date=2018-11-27&start_date=2018-11-26&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        assert r.json["success"] is True
        assert r.json["data"][str(alert.id)]["id"] == alert.id
        assert len(r.json["data"]) == 1


def test_get_notifications_as_csv_no_notifications(app, client, session):
    """
    Test that getting the notifications as a CSV returns 200.
    """
    user = users.UserFactory()
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()
    with app.app_context(), login(client, user):
        r = client.get(
            "/api/web/v1/notifications/get/as_csv?end_date=2018-11-27&start_date=2018-11-26&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.reader(r.get_data(as_text=True).splitlines())
        row = next(reader)

        assert row == [
            "id",
            "team_id",
            "received_dt",
            "notif_type",
            "body",
            "version",
            "node_name",
            "event_id",
            "ticket_id",
            "warnings",
            "errors",
            "status",
            "method",
            "provider",
            "provider_notif_id",
        ]

        with pytest.raises(StopIteration):
            next(reader)


def test_get_notifications_as_csv_one_notification(app, client, session):
    """
    Test that getting a notification as a CSV returns 200.
    """
    user = users.UserFactory(timezone="UTC")
    team = teams.hotpotato_team()
    user.teams_users.append(TeamsUsers(team=team, primary=False))
    session.commit()
    with app.app_context(), login(client, user):
        alert = notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 26, hour=11), team=team
        )
        notifications.NotificationFactory(
            notif_type="alert", received_dt=datetime(2018, 11, 25, hour=11), team=team
        )
        session.commit()
        r = client.get(
            "/api/web/v1/notifications/get/as_csv?end_date=2018-11-27&start_date=2018-11-26&team_id={}".format(
                team.id
            )
        )

        assert r.status_code == HTTPStatus.OK
        reader = csv.DictReader(r.get_data(as_text=True).splitlines())

        row = next(reader)
        assert row["id"] == str(alert.id)
        assert row["received_dt"] == "2018-11-26T11:00:00+00:00"
