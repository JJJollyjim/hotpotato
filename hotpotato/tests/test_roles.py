import pytest

from hotpotato import roles as role_functions
from hotpotato.tests import teams


def test_get(app, db):
    """
    test that get returns something
    """
    with app.app_context():
        team = teams.TeamFactory()
        db.session.commit()
        wrong_name = "fail"
        correct_name = "admin"
        role_functions.create(team.id, correct_name, "desc")
        role = role_functions.get_by_name(team.id, correct_name)
        role_id = role.id
        assert role_functions.get(role_id)
        assert role_id is not None
        assert role.name == correct_name
        with pytest.raises(role_functions.RoleNameError):
            role_functions.get_by_name(team.id, wrong_name)


def test_get_by(app, db):
    """
    check that the get_by function returns something
    """
    with app.app_context():
        roles = role_functions.get_by(name="*")
        assert roles is not None


def test_create(app, db):
    with app.app_context():
        team = teams.TeamFactory()
        db.session.commit()
        role = role_functions.create(team.id, "examp", "example user")

        assert role == role_functions.get_by_name(team.id, "examp")

        with pytest.raises(role_functions.RoleNameError):
            role_functions.get_by_name(team.id, "wrong_name")
