"""
Role unit test functions.
"""


from hotpotato import models


def create_defaults_for_team(team):
    return (
        models.Role(name="admin", description="Administrator", team_id=team.id),
        models.Role(name="user", description="User", team_id=team.id),
    )
