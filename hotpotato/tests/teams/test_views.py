from http import HTTPStatus as Status

from flask import url_for

from hotpotato import roles
from hotpotato.teams import hotpotato_team
from hotpotato.tests import users
from hotpotato.tests.conftest import login


def test_create(client, app, session):
    """
    Test that a team can be created.
    """

    user = users.UserFactory()
    user.roles.append(roles.get_by_name(hotpotato_team().id, "admin"))
    session.commit()

    with app.app_context(), login(client, user):
        r = client.post(
            url_for("views_admin.add_team"),
            data=dict(name="Test team", escalates_to=""),
            follow_redirects=True,
        )
        assert r.status_code == Status.OK
