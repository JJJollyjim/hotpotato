from hotpotato import teams as hotpotato_teams, views
from hotpotato.tests import teams


def test_cached_url_for(monkeypatch, app, session):
    """
    Test the behaviour of cached_url_for, specificially, that it behaves correctly
    when the team is changed.
    """

    team1 = teams.TeamFactory()
    team2 = teams.TeamFactory()
    session.commit()

    with app.app_context():
        # Clear the cache.
        # Tries the team decorated (inner_fn) version first, but if this fails,
        # try to call cache_clear on the function directly.
        try:
            views.cached_url_for.inner_fn.cache_clear()
        except AttributeError:
            views.cached_url_for.cache_clear()

        monkeypatch.setattr(hotpotato_teams, "current_team", team1)
        # Validity of the result on an empty cache.
        assert views.cached_url_for("views.main_site").endswith(
            "?team_id={}".format(team1.id)
        )
        # Validity of the cached result, same team.
        assert views.cached_url_for("views.main_site").endswith(
            "?team_id={}".format(team1.id)
        )

        # Now change the team, and see if the result changes accordingly.
        monkeypatch.setattr(hotpotato_teams, "current_team", team2)
        assert views.cached_url_for("views.main_site").endswith(
            "?team_id={}".format(team2.id)
        )
