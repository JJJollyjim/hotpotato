"""
Team unit test functions.
"""

import factory

from hotpotato import models
from hotpotato.models import db


class TeamFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.Team
        sqlalchemy_session = db.session

    name = factory.Faker("name")


def create_fake(fake):
    return models.Team(name=fake.word())
