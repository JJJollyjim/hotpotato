from hotpotato import teams
from hotpotato.models import TeamsUsers
from hotpotato.notifications import messages
from hotpotato.tests import notifications, oncall_contacts, servers, users


def test_check_failed_0(runner, session):
    """
    Run check-failed on an empty database and check that it exits without errors.
    """
    result = runner.invoke(args=["alert", "check-failed"])
    assert " 0 failed alerts" in result.output
    assert result.exit_code == 0


def test_check_failed(runner, session, app):
    """
    Run check-failed on a database with a failed alaert and check that it sends a message.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    oncall_contacts.OncallContactsFactory(user=oncall_user)
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"
    session.commit()

    count = len(messages.get_by())
    result = runner.invoke(args=["alert", "check-failed"])
    assert " 1 failed alerts" in result.output
    assert result.exit_code == 0
    assert len(messages.get_by()) > count


def test_check_failed_send_failures(runner, session, app):
    """
    Test messages are sent to contacts with send_failures enabled.
    """
    oncall_user = users.UserFactory()
    servers.ServerFactory()
    team = teams.hotpotato_team()
    team.on_call = oncall_user
    oncall_contacts.OncallContactsFactory(user=oncall_user)
    alert = notifications.NotificationFactory(notif_type="alert")
    alert.json["status"] = "SEND_FAILED"

    other_user = users.UserFactory()
    other_user.teams_users.append(TeamsUsers(team=team, primary=False))
    oncall_contacts.OncallContactsFactory(user=other_user, send_failures=True)
    session.commit()

    count = len(messages.get_by())
    result = runner.invoke(args=["alert", "check-failed"], catch_exceptions=False)
    assert result.exit_code == 0
    assert "Notifying" in result.output
    assert len(messages.get_by()) == count + 2
