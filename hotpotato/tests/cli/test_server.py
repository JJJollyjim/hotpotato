def test_check_missed_hbeats(runner, session):
    """
    Test check-missed-hbeats runs without any errors.
    """
    result = runner.invoke(args=["server", "check-missed-hbeats"])
    assert result.exit_code == 0


def test_get_monitored(runner, session):
    """
    Test get-monitored runs without any errors.
    """
    result = runner.invoke(args=["server", "get-monitored"])
    assert result.exit_code == 0
