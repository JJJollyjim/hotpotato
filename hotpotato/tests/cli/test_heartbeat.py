def test_delete_old(runner, session):
    """
    Test delete-old runs without any errors.
    """
    result = runner.invoke(args=["heartbeat", "delete-old"])
    assert result.exit_code == 0
