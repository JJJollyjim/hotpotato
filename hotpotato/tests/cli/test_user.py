import faker
import flask_security.utils

from hotpotato import roles, teams, users as hotpotato_users
from hotpotato.tests import users


def test_user_create_admin(runner, session):
    """
    Test creating an admin user.
    """

    fake = faker.Faker()
    email = fake.email()
    while hotpotato_users.get_by(email=email):
        email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "--admin",
        ]
    )
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.name == name
    assert user.active is True
    assert roles.get_by_name(teams.hotpotato_team().id, "admin") in user.roles
    assert flask_security.utils.verify_password(password, user.password)


def test_user_create_not_active(runner, session):
    """
    Test creating an inactive user.
    """

    fake = faker.Faker()
    email = fake.email()
    while hotpotato_users.get_by(email=email):
        email = fake.email()
    name = fake.name()
    password = "password"

    result = runner.invoke(
        args=[
            "user",
            "create",
            str(teams.hotpotato_team().id),
            name,
            email,
            "--password",
            password,
            "--not-active",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.name == name
    assert user.active is False
    assert flask_security.utils.verify_password(password, user.password)


def test_user_activate(runner, session):
    """
    Test activating a user.
    """

    user = users.UserFactory()
    email = user.email

    user.active = False
    session.commit()

    result = runner.invoke(args=["user", "activate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is True


def test_user_deactivate(runner, session):
    """
    Test deactivating a user.
    """

    user = users.UserFactory()
    email = user.email

    user.active = True
    session.commit()

    result = runner.invoke(args=["user", "deactivate", user.email])
    assert result.exit_code == 0

    user = hotpotato_users.get_by_email(email)
    assert user.active is False
