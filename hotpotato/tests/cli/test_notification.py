import datetime

from hotpotato.notifications import alerts, handovers, messages
from hotpotato.tests import oncall_contacts, servers, teams, users


def test_get(runner, session):
    """
    Test get runs without any errors.
    """
    user = users.UserFactory()
    session.commit()

    message = messages.create(team_id=None, body="test", to_user_id=user.id)
    result = runner.invoke(args=["notification", "get", str(message.id)])
    assert result.exit_code == 0


def test_alert(runner, session):
    """
    Test alert sends an alert.
    """
    user = users.UserFactory()
    team = teams.TeamFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    server = servers.ServerFactory()
    session.commit()

    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    count = len(alerts.get_by(output="Something is probably broken"))
    result = runner.invoke(
        args=[
            "notification",
            "send",
            "alert",
            str(team.id),
            str(user.id),
            "host",
            str(server.id),
            "TEST123",
            "example.com",
            "Example Wesbite",
            "Example Service",
            "CRITICAL",
            "Something is probably broken",
            timestamp,
        ]
    )
    assert result.exit_code == 0
    assert len(alerts.get_by(output="Something is probably broken")) > count


def test_handover(runner, session):
    """
    Test handover sends a handover notification.
    """
    team = teams.TeamFactory()
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()

    user_id = user.id
    # WARNING: This assignment is required to avoid the team object expiring
    team_id = team.id

    count = len(handovers.get_by(team_id=team_id))
    result = runner.invoke(
        args=["notification", "send", "handover", str(team.id), str(user_id)]
    )
    assert result.exit_code == 0
    assert len(handovers.get_by(team_id=team_id)) > count


def test_message(runner, session):
    """
    Test message sends a message.
    """
    user = users.UserFactory()
    oncall_contacts.OncallContactsFactory(user=user)
    session.commit()

    user_id = user.id

    count = len(messages.get_by(user_id=user_id))
    result = runner.invoke(
        args=["notification", "send", "message", "Test", str(user_id)]
    )
    assert result.exit_code == 0
    assert len(messages.get_by(user_id=user_id)) > count
