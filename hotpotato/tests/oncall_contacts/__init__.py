"""
On-call contact unit test functions.
"""


import string

import factory

from hotpotato import models, oncall_contacts
from hotpotato.models import db
from hotpotato.tests import users, util


class OncallContactsFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.OncallContact
        sqlalchemy_session = db.session

    user = factory.SubFactory(users.UserFactory)
    method = factory.Faker("random_element", elements=oncall_contacts.METHOD)

    @factory.lazy_attribute
    def contact(self):
        if self.method == "sms" or self.method == "pager":
            return self.fake_number
        elif self.method == "pushover":
            return "".join(self.fake_elements)

    verifiable = False
    priority = 0
    send_pages = True
    send_failures = False

    class Params:
        fake_number = factory.Faker("phone_number")
        fake_elements = factory.Faker(
            "random_elements",
            elements=tuple(string.ascii_lowercase + string.digits),
            length=30,
        )


def create_fake(fake):
    """
    Generate a fake on-call contact, with randomly set data.
    """

    method = fake.random.choice(tuple(oncall_contacts.METHOD))

    # if method == "app": # TODO: app
    if method == "sms" or method == "pager":
        contact = fake.phone_number()
    elif method == "pushover":
        contact = "".join(
            fake.random_elements(
                tuple(string.ascii_lowercase + string.digits), length=30
            )
        )

    return models.OncallContact(
        user_id=util.get_random_object(models.db.session, models.User).id,
        method=method,
        contact=contact,
        send_pages=fake.boolean(),
        send_failures=fake.boolean(),
    )
