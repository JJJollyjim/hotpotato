from datetime import datetime, time

import pytest
import pytz
import workalendar

import hotpotato.api.stats.v1.stats as stats
from hotpotato.tests.notifications import NotificationFactory
from hotpotato.tests.teams import TeamFactory

# Around January, when the test data are, this is UTC+13
nz_tz = pytz.timezone("Pacific/Auckland")
nz_cal = workalendar.registry.registry.get_calendar_class("NZ")()


def test_lookup_none(session):
    team = TeamFactory()
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_none_on_team(session):
    team = TeamFactory()
    NotificationFactory(notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0))
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_not_alert(session):
    team = TeamFactory()
    NotificationFactory(
        team=team, notif_type="handover", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    NotificationFactory(
        team=team, notif_type="message", received_dt=datetime(2019, 1, 1, 4, 21, 0)
    )
    session.commit()

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz)


def test_lookup_simple(session):
    team = TeamFactory()
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    session.commit()

    assert datetime(2019, 1, 1, 17, 20, 0) == stats.last_matching_alert_datetime(
        team.id, nz_tz
    ).replace(tzinfo=None)


def test_lookup_out_of_hours(session):
    team = TeamFactory()
    # This alert is just before we start work on the 3rd of January NZ time
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 2, 20, 29, 4)
    )

    # We work from 9:30am to 9:35am (nz time), so we don't mind the alerts during that time
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 1, 20, 30, 1)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 4, 20, 31, 40)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 8, 20, 33, 0)
    )
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 9, 20, 34, 59)
    )
    session.commit()

    assert datetime(2019, 1, 3, 9, 29, 4) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35))
    ).replace(tzinfo=None)


def test_lookup_ooh_and_calendar(session):
    team = TeamFactory()
    # This alert is after we finish work on the 3rd of January NZ time (which is a working day)
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 3, 5, 29, 4)
    )

    # We work from 9:30am to 9:35am (nz time), so we don't mind the alerts during that time (since they're on weekdays)
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 6, 20, 31, 40)
    )
    session.commit()

    assert datetime(2019, 1, 3, 18, 29, 4) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)

    # This alert is during working hours, but it's on Waitangi day!
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 2, 5, 20, 31, 46)
    )
    session.commit()

    assert datetime(2019, 2, 6, 9, 31, 46) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)

    # This alert is out of working hours, on Waitangi day!
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 2, 5, 21, 0, 0)
    )
    session.commit()

    assert datetime(2019, 2, 6, 10, 0, 0) == stats.last_matching_alert_datetime(
        team.id, nz_tz, business_hours=(time(9, 30), time(9, 35)), cal=nz_cal
    ).replace(tzinfo=None)


def test_lookup_ignore_server_ids(session):
    team = TeamFactory()
    notif = NotificationFactory(
        notif_type="alert", received_dt=datetime(2019, 1, 1, 4, 20, 0)
    )
    session.commit()

    sid = notif.json["server_id"]

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(team.id, nz_tz, ignore_server_ids=[sid])

    with pytest.raises(StopIteration):
        stats.last_matching_alert_datetime(
            team.id, nz_tz, ignore_server_ids=[sid, 1234]
        )


def test_api_none(app, client, session):
    app.config["STATS_API_KEY"] = "foo"

    team = TeamFactory()
    session.commit()

    resp = client.get(
        "/api/stats/v1/days_since_alert/{}?tz=Pacific/Auckland".format(team.id),
        headers={"Authorization": "Bearer foo"},
    )
    assert resp.status_code == 204


def test_api_simple(app, client, session, monkeypatch):
    app.config["STATS_API_KEY"] = "foo"

    team = TeamFactory()
    # Occurs at the end of the 13th in NZ
    NotificationFactory(
        team=team, notif_type="alert", received_dt=datetime(2019, 1, 13, 10, 58, 0)
    )
    session.commit()

    resp = client.get(
        "/api/stats/v1/days_since_alert/{}?tz=Pacific/Auckland".format(team.id),
        headers={"Authorization": "Bearer foo"},
    )
    assert resp.status_code == 200
    # I wrote this test 60 days after the date above
    assert int(resp.data) > 60


def test_api_all_args(app, client, session, monkeypatch):
    app.config["STATS_API_KEY"] = "foo"

    team = TeamFactory()
    session.commit()

    # Just test arg parsing
    resp = client.get(
        "/api/stats/v1/days_since_alert/{}?tz=Pacific/Auckland&region=NZ&business_hours=9-5:30&ignore_servers=123,345".format(
            team.id
        ),
        headers={"Authorization": "Bearer foo"},
    )
    assert 200 <= resp.status_code < 300
