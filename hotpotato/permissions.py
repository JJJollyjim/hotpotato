from functools import wraps

from flask import current_app, request
from flask_login.config import EXEMPT_METHODS
from flask_security import current_user
from flask_security.decorators import _get_unauthorized_view

from hotpotato.teams import current_team, hotpotato_team


def login_required(fn):
    """
    Replacement for flask_security's login_required decorator. This checks that
    the user is in the current team.
    """

    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if request.method in EXEMPT_METHODS:
            return fn(*args, **kwargs)
        elif current_app.config.get("LOGIN_DISABLED"):
            return fn(*args, **kwargs)

        if current_user.is_anonymous:
            return current_app.login_manager.unauthorized()

        if current_team in current_user.teams:
            return fn(*args, **kwargs)

        # This is required for routes exempt from the redirect, for example
        # /api/web routes. Anything not exempt is redirected before this is
        # called.
        if not current_team:
            return fn(*args, **kwargs)

        if hasattr(current_app.extensions["security"], "_get_unauthorized_callback"):
            return current_app.extensions["security"]._get_unauthorized_callback()
        return _get_unauthorized_view()

    return decorated_view


def roles_accepted(*roles, hotpotato=False):
    """
    Replacement for flask_security's roles_accepted decorator to take into account teams.
    """

    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if hotpotato:
                user_roles = [
                    r for r in current_user.roles if r.team_id == hotpotato_team().id
                ]
            else:
                user_roles = [
                    r for r in current_user.roles if r.team_id == current_team.id
                ]

            for role in roles:
                if role in user_roles:
                    return fn(*args, **kwargs)

            if hasattr(
                current_app.extensions["security"], "_get_unauthorized_callback"
            ):
                return current_app.extensions["security"]._get_unauthorized_callback()
            return _get_unauthorized_view()

        return decorated_view

    return wrapper
