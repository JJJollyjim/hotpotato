"""
Server functions.
"""


from datetime import datetime

import pytz
import sqlalchemy

from hotpotato import heartbeats
from hotpotato.models import Server as Model, db

# Heartbeat frequency, in seconds.
# Ideally should be configurable.
HBEAT_FREQ = 60


class ServerError(Exception):
    """
    Server exception base class.
    """

    pass


class ServerCreateError(Exception):
    """
    Server create exception class.
    """

    pass


class ServerIDError(ServerError):
    """
    Exception for invalid server ID.
    """

    def __init__(self, server_id):
        self.server_id = server_id
        super().__init__(
            "Invalid server ID (or unable to find server with ID): {}".format(server_id)
        )


class ServerAPIKeyError(ServerError):
    """
    Exception for invalid server API key.
    """

    def __init__(self, api_key):
        self.api_key = api_key
        super().__init__(
            "Invalid server API key (or unable to find server with API key): {}".format(
                api_key
            )
        )


class ServerTimezoneError(ServerError):
    """
    Exception for invalid server timezone.
    """

    def __init__(self, server_id, timezone):
        self.server_id = server_id
        self.timezone = timezone
        super().__init__(
            "Invalid timezone for server with ID {}: {}".format(
                self.server_id, self.timezone
            )
        )


def get(server_id):
    """
    Get a server object using the given ID and return it.
    """

    try:
        return Model.query.filter_by(id=server_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise ServerIDError(server_id)


def get_by(**kwargs):
    """
    Return a tuple containing all server object matching the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def get_by_api_key(api_key):
    """
    Get a server object using its API key and return it.
    """

    obj = Model.query.filter_by(apikey=api_key).first()

    if not obj:
        raise ServerAPIKeyError(api_key)

    return obj


def missed_hbeats_query(now, hbeat_freq):
    """
    Return an SQL query object for finding servers have not sent heartbeats
    within their threshold time, or have never sent a heartbeat.
    """
    subquery_last_hbeats = heartbeats.get_last_query().subquery()
    return (
        db.session.query(
            Model.hostname,
            Model.timezone,
            Model.enable_heartbeat,
            Model.disable_missed_heartbeat_notif,
            subquery_last_hbeats.c.last_hbeat,
            Model.id,
            Model.link,
        )
        .outerjoin(subquery_last_hbeats, subquery_last_hbeats.c.server_id == Model.id)
        .filter(
            Model.enable_heartbeat == sqlalchemy.sql.expression.true(),
            Model.disable_missed_heartbeat_notif == sqlalchemy.sql.expression.false(),
            sqlalchemy.or_(
                sqlalchemy.sql.expression.cast(
                    now - subquery_last_hbeats.c.last_hbeat, sqlalchemy.types.Integer
                )
                >= Model.missed_heartbeat_limit * hbeat_freq,
                subquery_last_hbeats.c.server_id.is_(None),
            ),
        )
    )


def get_num_missed_hbeats():
    """
    Return the number of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.
    """

    now = datetime.utcnow()

    return missed_hbeats_query(now, HBEAT_FREQ).count()


def get_missed_hbeats():
    """
    Return a list of servers which have not sent heartbeats within their threshold time,
    or have never sent a heartbeat.
    """

    now = datetime.utcnow()

    missed_servs_res = missed_hbeats_query(now, HBEAT_FREQ).all()

    return missed_servs_res


def get_monitored():
    """
    Get a list of all the servers that are currently being actively monitored.
    Does not list servers whose notifications have temporarily been disabled, therefore
    it does not constitute a "list of servers that we care about on an on-going basis".
    """
    return Model.query.filter_by(
        enable_heartbeat=True, disable_missed_heartbeat_notif=False
    ).all()


# pylint: disable=too-many-arguments
def create(
    hostname,
    apikey,
    timezone,
    link,
    enable_heartbeat=True,
    disable_missed_heartbeat_notif=False,
    missed_heartbeat_limit=5,
):
    """
    Create a new server,  and add it to the database, and return it
    """

    if get_by(hostname=hostname):
        raise ServerCreateError(
            'Server with hostname "{}" already exists'.format(hostname)
        )

    if get_by(apikey=apikey):
        raise ServerCreateError(
            'Server with API key "{}" already exists'.format(apikey)
        )

    server = Model(
        hostname=hostname,
        apikey=apikey,
        enable_heartbeat=enable_heartbeat,
        disable_missed_heartbeat_notif=disable_missed_heartbeat_notif,
        missed_heartbeat_limit=missed_heartbeat_limit,
        timezone=timezone,
        link=link,
    )

    db.session.add(server)
    db.session.commit()

    return server


def timezone_get_as_timezone(tz_str):
    """
    Return the given timezone string as a timezone object.
    """

    return pytz.timezone(tz_str)
