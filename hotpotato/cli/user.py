"""
Command line interface for adding and removing users.
"""


import click
import flask
import flask.cli
import pytz

from hotpotato import roles, teams, users
from hotpotato.models import db, user_datastore


@click.group("user", cls=flask.cli.AppGroup)
def user():
    """
    User commands.
    """

    pass


@user.command("create")
@click.argument("team_id", type=click.INT)
@click.argument("name", type=click.STRING)
@click.argument("email", type=click.STRING)
@click.option("-t", "--timezone", default="UTC")
@click.option("-na", "--not-active", is_flag=True)
@click.option("-A", "--admin", is_flag=True)
@click.password_option()
def create(team_id, name, email, password, timezone, not_active, admin):
    """
    Create a user.
    """

    if timezone not in pytz.all_timezones:
        raise click.UsageError("Unknown timezone")

    try:
        team = teams.get((int(team_id)))
    except teams.TeamIDError:
        raise click.BadParameter("Team not found.")

    try:
        new_user = users.create(email, password, name, timezone, team, not not_active)
    except users.DuplicateEmailError:
        raise click.BadParameter("A user with this email already exists.")

    if admin:
        role_admin = roles.get_by_name(int(team_id), "admin")
        new_user.add_role(role_admin)

    db.session.commit()
    click.echo("Created new user with email {}.".format(new_user.email))


@user.command("deactivate")
@click.argument("email", type=click.STRING)
def deactivate(email):
    """
    Deactivate a user.
    """
    try:
        user = users.get_by_email(email)

        if user_datastore.deactivate_user(user):
            user_datastore.commit()
            click.echo("Deactivated user with email {}.".format(user.email))
        else:
            click.echo("User with email {} already deactivated.".format(user.email))
    except users.UserEmailError:
        raise click.UsageError("User does not exist")


@user.command("activate")
@click.argument("email", type=click.STRING)
def activate(email):
    """
    Activate a user.
    """
    try:
        user = users.get_by_email(email)

        if user_datastore.activate_user(user):
            user_datastore.commit()
            click.echo("Activated user with email {}.".format(user.email))
        else:
            click.echo("User with email {} already activated.".format(user.email))
    except users.UserEmailError:
        raise click.UsageError("User does not exist")
