"""
Command line interface to create database.
"""


import click
import flask
import flask.cli

from hotpotato import models, roles, teams, users


@click.group("database", cls=flask.cli.AppGroup)
def database():
    """
    Database commands.
    """

    pass


@database.command("create")
def create():
    """
    Create the database
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)


@database.command("create-dev")
def create_dev():
    """
    Create the database, with test users set up
    """
    models.create(flask.current_app)
    models.initialise(flask.current_app)

    with flask.current_app.app_context():
        team = teams.hotpotato_team()
        user_test1 = users.create(
            "test1@example.com", "test_password1", "Test User 1", "UTC", team
        )
        user_test2 = users.create(
            "test2@example.com", "test_password2", "Test User 2", "UTC", team
        )

        role_admin = roles.get_by_name(team.id, "admin")
        user_test1.add_role(role_admin)
        user_test2.add_role(role_admin)
        models.db.session.commit()
