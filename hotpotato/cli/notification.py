"""
Command line interface (CLI) notification functions.
"""


import click
import flask.cli
from flask import current_app

from hotpotato import servers, util
from hotpotato.notifications import alerts, handovers, messages, notifications


@click.group("notification", cls=flask.cli.AppGroup)
def notification():
    """
    Notification commands.
    """

    pass


@notification.command("get")
@click.argument("notif_id", type=click.INT)
def get(notif_id):
    """
    Get a notification.
    """

    click.echo(notifications.get(notif_id).as_json(indent=2))


@notification.group("send")
def send():
    """
    Sending commands.
    """

    pass


# pylint: disable=too-many-arguments
@send.command("alert")
@click.argument("team_id", type=click.INT)
@click.argument("user_id", type=click.INT)
@click.argument("alert_type", type=click.Choice(alerts.TYPE))
@click.argument("server_id", type=click.INT)
@click.argument("trouble_code", type=click.STRING)
@click.argument("hostname", type=click.STRING)
@click.argument("display_name", type=click.STRING)
@click.argument("service_name", type=click.STRING)
@click.argument("state", type=click.STRING)
@click.argument("output", type=click.STRING)
@click.argument("timestamp", type=click.STRING)
def alert(
    team_id,
    user_id,
    alert_type,
    server_id,
    trouble_code,
    hostname,
    display_name,
    service_name,
    state,
    output,
    timestamp,
):
    """
    Send an alert.
    """
    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    alerts.create(
        team_id=team_id,
        user_id=user_id,
        alert_type=alert_type,
        server=servers.get(server_id),
        trouble_code=trouble_code,
        hostname=hostname,
        display_name=display_name,
        service_name=service_name,
        state=state,
        output=output,
        timestamp=util.datetime_get_from_string(timestamp),
    ).send(run_async=False)


@send.command("handover")
@click.argument("team_id", type=click.INT)
@click.argument("to_user_id", type=click.INT)
@click.option(
    "--from-user-id",
    type=click.INT,
    default=None,
    help="ID of the current on-call person.",
)
@click.option(
    "--message",
    "handover_message",
    type=click.STRING,
    default=None,
    help="Message to send to the involved users.",
)
def handover(team_id, to_user_id, from_user_id, handover_message):
    """
    Send a handover.
    """
    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    to_handover, from_handover = handovers.create(
        team_id=team_id,
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        message=handover_message,
    )

    to_handover.send(run_async=False)
    if from_handover:
        from_handover.send(run_async=False)


@send.command("message")
@click.argument("body", type=click.STRING)
@click.argument("to_user_id", type=click.INT)
@click.option("--team-id", type=click.INT, default=None)
@click.option(
    "--from-user-id",
    type=click.INT,
    default=None,
    help="User ID to mark as the author.",
)
@click.option(
    "--reply-to-notif-id",
    type=click.INT,
    default=None,
    help="Notification ID to reply to.",
)
def message(team_id, body, to_user_id, from_user_id, reply_to_notif_id):
    """
    Send a message.
    """
    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    messages.create(
        team_id=team_id,
        body=body,
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        reply_to_notif_id=reply_to_notif_id,
    ).send(run_async=False)
