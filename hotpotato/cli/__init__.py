"""
Command line interface (CLI) command groups and functions.
"""


from hotpotato.cli.alert import alert
from hotpotato.cli.database import database
from hotpotato.cli.heartbeat import heartbeat
from hotpotato.cli.hotpotato import hotpotato
from hotpotato.cli.notification import notification
from hotpotato.cli.server import server
from hotpotato.cli.server_uptime import server_uptime
from hotpotato.cli.test import test
from hotpotato.cli.user import user


def init_app(app):
    """
    Initialise the Flask app CLI with Hot Potato-related commands.
    """

    app.cli.add_command(hotpotato)
    app.cli.add_command(heartbeat)
    app.cli.add_command(notification)
    app.cli.add_command(alert)
    app.cli.add_command(server)
    app.cli.add_command(server_uptime)
    app.cli.add_command(test)
    app.cli.add_command(database)
    app.cli.add_command(user)

    # Disable the Flask-Security built-in user and role management commands,
    # because we have Hot Potato ones built into the CLI.
    app.config["SECURITY_CLI_USERS_NAME"] = False
    app.config["SECURITY_CLI_ROLES_NAME"] = False
