"""
Command line interface (CLI) server functions.
"""


import click
import flask.cli
from flask import current_app

from hotpotato import servers, teams, util
from hotpotato.notifications import messages


@click.group("server", cls=flask.cli.AppGroup)
def server():
    """
    Server commands.
    """

    pass


@server.command("check-missed-hbeats")
def check_missed_hbeats():
    """
    Run the check for missed heartbeats.
    """

    current_app.config["SERVER_NAME"] = current_app.config["HOTPOTATO_WEBUI_URL"]

    servs = servers.get_missed_hbeats()

    click.echo("There are {} missed heartbeats".format(len(servs)))

    if not servs:
        return

    oncall_user = teams.hotpotato_team().oncall

    click.echo("Paging on-call person {}".format(oncall_user.name))

    for serv in servs:
        body = "{}: HOTPOTATO002 Intervention required, {} ".format(
            util.node_name, serv.hostname
        )

        if serv["last_hbeat"]:
            body += "has not sent heartbeats for the last {} seconds ".format(
                int(serv.last_hbeat_ago.total_seconds())
            )
        else:
            body += "has never sent a heartbeat "

        body += "(threshold is {} heartbeats, or {} seconds)".format(
            serv.missed_heartbeat_limit,
            int(servers.HBEAT_FREQ * serv.missed_heartbeat_limit),
        )

        click.echo(body)

        # TODO: replace message with alert of some kind
        messages.create(
            team_id=teams.hotpotato_team().id, body=body, to_user_id=oncall_user.id
        ).send(run_async=False)


@server.command("get-monitored")
def get_monitored():
    """
    List all servers currently being monitored.
    """

    for serv in servers.get_monitored():
        click.echo(serv.hostname)
