"""
Account views blueprint.
"""


import flask

blueprint = flask.Blueprint("views_account", __name__)
