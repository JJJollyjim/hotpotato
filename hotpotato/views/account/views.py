from flask import current_app, flash, redirect, render_template, request, url_for
from flask_security import current_user

from hotpotato import oncall_contacts, teams, users
from hotpotato.models import db
from hotpotato.notifications import messages
from hotpotato.permissions import login_required
from hotpotato.views.account._blueprint import blueprint
from hotpotato.views.account.forms import AddContactForm, TimezoneForm


@blueprint.route("/", methods=["GET"])
@login_required
def manage_account(add_contact_form=None):
    """
    Page to view account information
    """
    if add_contact_form is None:
        add_contact_form = AddContactForm()

    timezone_form = TimezoneForm()

    verifiable_contacts = tuple(
        oncall_contacts.get_for_user(current_user, verifiable=True)
    )
    unverifiable_contacts = tuple(
        oncall_contacts.get_for_user(current_user, verifiable=False)
    )

    return render_template(
        "account/settings.html",
        user=current_user,
        verifiable_contacts=verifiable_contacts,
        unverifiable_contacts=unverifiable_contacts,
        timezone_form=timezone_form,
        add_contact_form=add_contact_form,
    )


@blueprint.route("/remove-contact-method", methods=["POST"])
@login_required
def remove_contact_methods():
    for item in request.form:
        if item == "csrf_token":
            continue
        try:
            oncall_contacts.delete_by_id(item)
        except oncall_contacts.OncallContactIDError:
            db.session.rollback()
    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/manage_team", methods=["POST"])
@login_required
def account_team():
    f = request.form

    if f["action"] == "set_primary":
        try:
            team = teams.get(f["team_id"])
        except teams.TeamIDError:
            flash("The team you are trying to set as primary doesn't exist", "error")
            return redirect(url_for("views_account.manage_account"))

        if team not in current_user.teams:
            flash("You can't set a team your not a member of as primary", "error")
            return redirect(url_for("views_account.manage_account"))

        users.set_primary_team(current_user, f["team_id"])
        return redirect(url_for("views_account.manage_account"))

    if f["action"] == "leave":
        try:
            team = teams.get(f["team_id"])
        except teams.TeamIDError:
            flash("The team you are trying to leave doesn't exist", "error")
            return redirect(url_for("views_account.manage_account"))

        if current_user.primary_team == teams.get(f["team_id"]):
            flash("You can't leave your primary team.", "error")
            return redirect(url_for("views_account.manage_account"))
        current_user.teams.remove(teams.get(f["team_id"]))
        db.session.commit()
        return redirect(url_for("views_account.manage_account"))

    flash("Unknown action", "error")
    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/tz", methods=["POST"])
@login_required
def change_timezone():
    """
    Changes the preffered timezone of the user
    """

    form = TimezoneForm()

    if form.validate_on_submit():
        current_user.timezone = form.timezone.data
        db.session.commit()

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/add-contact-method", methods=["GET", "POST"])
@login_required
def add_contact_method():
    """
    Page to allow adding contact methods for on-call people.
    """
    form = AddContactForm()
    if form.validate_on_submit():
        # If the contact method doesn't exist, create it.
        oncall_contact = oncall_contacts.create(
            user_id=current_user.id,
            contact=form.contact.data,
            method=form.method.data,
            priority=form.priority.data,
            send_pages=form.send_pages.data,
            send_failures=form.send_failures.data,
        )

        # Send a test message if the contact method is SMS.
        if form.method.data == "sms" or form.method.data == "pushover":
            messages.create(
                team_id=None, body="This is a test message", to_user_id=current_user.id
            ).send(contact=oncall_contact)

        return redirect(url_for("views_account.manage_account"))

    return manage_account(add_contact_form=form)


@blueprint.route("/toggle-send-pages/<contact_id>", methods=["POST"])
@login_required
def toggle_send_pages(contact_id):
    """
    Toggles the sending of pages to a contact method
    """
    try:
        oncall_contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError:
        flash("Contact method doesn't exist", "error")
        return redirect(url_for("views_account.manage_account"))

    oncall_contact.send_pages = not oncall_contact.send_pages
    db.session.commit()

    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/toggle-send-failures/<contact_id>", methods=["POST"])
@login_required
def toggle_notify_on_fail(contact_id):
    """
    Toggle sending failure alerts to a contact method.
    """

    try:
        oncall_contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError:
        flash("Contact method doesn't exist", "error")
        return redirect(url_for("views_account.manage_account"))

    if oncall_contact.send_failures:
        current_app.logger.info(
            "Removing failure alerts for {}".format(current_user.name)
        )

        if oncall_contact.method != "pager":
            messages.create(
                team_id=None,
                body="You will no longer receive failure alerts",
                to_user_id=current_user.id,
            ).send(contact=oncall_contact)

        oncall_contact.send_failures = False
        db.session.commit()

    else:
        if teams.hotpotato_team() not in current_user.teams.all():
            current_app.logger.info(
                "Refused to add failure alerts for {}".format(current_user.name)
            )
            flash(
                "You must be in the {} team to recieve failure alerts".format(
                    teams.HOTPOTATO_TEAM_NAME
                ),
                "error",
            )
            return redirect(url_for("views_account.manage_account"))

        current_app.logger.info(
            "Adding failure alerts for {}".format(current_user.name)
        )

        oncall_contact.send_failures = True
        db.session.commit()

        # Send a notification notifying the user they will get failure notifications on this
        # on-call contact.
        if oncall_contact.method != "pager":
            messages.create(
                team_id=None,
                body="You will now get failure alerts",
                to_user_id=current_user.id,
            ).send(contact=oncall_contact)
    return redirect(url_for("views_account.manage_account"))


@blueprint.route("/send-test-message/<contact_id>", methods=["POST"])
@login_required
def send_test_message(contact_id):
    """
    Sends a test message to a contact method.
    """

    try:
        oncall_contact = oncall_contacts.get(contact_id)
    except oncall_contacts.OncallContactIDError:
        flash("Contact method doesn't exist", "error")
        return redirect(url_for("views_account.manage_account"))

    messages.create(
        team_id=None, body="This is a test message", to_user_id=current_user.id
    ).send(contact=oncall_contact)

    flash("A test message has been sent to {}".format(oncall_contact.contact), "info")

    return redirect(url_for("views_account.manage_account"))
