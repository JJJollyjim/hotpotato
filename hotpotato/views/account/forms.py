import pytz
import requests
import wtforms
from flask import current_app
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError

from hotpotato import oncall_contacts, teams, util


class AddContactForm(FlaskForm):
    method = wtforms.SelectField("Contact Method")
    contact = wtforms.StringField("Contact Identifier", validators=[DataRequired()])
    priority = wtforms.IntegerField("Priority")
    send_pages = wtforms.BooleanField("Send Pages", false_values=None)
    send_failures = wtforms.BooleanField(
        "Send notification failure alerts", false_values=None
    )

    def __init__(self, **kwargs):
        super(AddContactForm, self).__init__(**kwargs)
        self.method.choices = [
            (method, method) for method in util.enabled_sending_methods()
        ]

    def validate_contact(self, field):
        if oncall_contacts.exists(
            user_id=current_user.id, contact=self.contact.data, method=self.method.data
        ):
            raise ValidationError("Contact method already exists")
        # Verify that the pushover token is valid
        if self.method.data == "pushover":
            token = current_app.config["PUSHOVER_API_TOKEN"]
            verify = requests.post(
                "https://api.pushover.net/1/users/validate.json",
                json={"token": token, "user": self.contact.data},
            )
            if verify.status_code != 200 or verify.json()["status"] != 1:
                raise ValidationError("pushover token is invalid")

    def validate_priority(self, field):
        # check for conflict in priority order
        verifiable = oncall_contacts.method_is_verifiable(self.method.data)
        if self.priority.data is None:
            return
        if oncall_contacts.exists(
            user_id=current_user.id, verifiable=verifiable, priority=self.priority.data
        ):
            raise ValidationError(
                "{} contact method with the priority value {} already exists".format(
                    "A verifiable" if verifiable else "An unverifiable",
                    self.priority.data,
                )
            )

    def validate_send_failures(self, field):
        if self.send_failures.data is None:
            return
        if self.send_failures.data and teams.hotpotato_team() not in current_user.teams:
            raise ValidationError(
                "You must be in the {} team to receive failure alerts".format(
                    teams.HOTPOTATO_TEAM_NAME
                )
            )


class TimezoneForm(FlaskForm):
    timezone = wtforms.SelectField(
        "Timezone",
        choices=[(timezone, timezone) for timezone in pytz.all_timezones],
        validators=[DataRequired()],
    )

    def __init__(self, **kwargs):
        super(TimezoneForm, self).__init__(**kwargs)
        if self.timezone.data == "None":
            self.timezone.data = current_user.timezone
