from hotpotato.views.account import views
from hotpotato.views.account._blueprint import blueprint

__all__ = ["views", "blueprint"]
