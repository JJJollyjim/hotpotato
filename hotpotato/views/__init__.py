import functools

import flask_security
import pkg_resources
from flask import url_for
from flask_wtf.csrf import CSRFProtect

from hotpotato import roles, servers as servers_module, teams, users, util
from hotpotato.functions import to_local_time
from hotpotato.notifications import (
    alerts,
    handovers,
    messages,
    notifications as notifications_class,
)
from hotpotato.views import (
    _blueprint as views_blueprint,
    account as views_account,
    admin as views_admin,
    notifications as views_notifications,
    servers as views_servers,
    views,
)

__all__ = ["views", "init_app"]


csrf = CSRFProtect()


@teams.team_decorator
@functools.lru_cache(maxsize=256)
def cached_url_for(*args, **kwargs):
    return url_for(*args, **kwargs)


def init_app(app):
    """
    Initialise the Flask app with the Hot Potato view blueprints.
    """

    csrf.init_app(app)

    # Set the template and static file folders in the app to their correct values.
    app.template_folder = pkg_resources.resource_filename("hotpotato", "templates")
    app.static_folder = pkg_resources.resource_filename("hotpotato", "static")

    # Add additional variables and macros to the app's Jinja2 environment.
    app.jinja_env.globals.update(
        notifications=notifications_class,
        alerts=alerts,
        handovers=handovers,
        messages=messages,
        servers=servers_module,
        users=users,
        roles=roles,
        util=util,
        node_name=util.node_name,
        current_user=flask_security.current_user,
        current_team=teams.current_team,
        to_local_time=to_local_time,
        HOTPOTATO_TEAM_NAME=teams.HOTPOTATO_TEAM_NAME,
        hotpotato_team=teams.hotpotato_team,
        snake_to_sentence=lambda s: s.replace("_", " ").capitalize(),
        url_for=cached_url_for,
    )

    # Add hard-coded configuration values.
    app.config["SECURITY_MSG_USER_DOES_NOT_EXIST"] = (
        "Incorrect username or password",
        "error",
    )
    app.config["SECURITY_MSG_INVALID_PASSWORD"] = (
        "Incorrect username or password",
        "error",
    )

    app.register_blueprint(views_blueprint.blueprint, url_prefix="/")
    app.register_blueprint(views_servers.blueprint, url_prefix="/servers")
    app.register_blueprint(views_account.blueprint, url_prefix="/account")
    app.register_blueprint(views_notifications.blueprint, url_prefix="/notifications")
    app.register_blueprint(views_admin.blueprint, url_prefix="/admin")
