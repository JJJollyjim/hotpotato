"""
Servers views blueprint.
"""


import flask

blueprint = flask.Blueprint("views_servers", __name__)
