from datetime import datetime

from flask import abort, jsonify, redirect, render_template, request, url_for

from hotpotato import heartbeats, servers
from hotpotato.api.server import key as api_server_key
from hotpotato.models import Server, db
from hotpotato.permissions import login_required, roles_accepted
from hotpotato.servers import (
    get_missed_hbeats as get_missed_hbeats_servs,
    get_num_missed_hbeats,
)
from hotpotato.views.servers._blueprint import blueprint
from hotpotato.views.servers.forms import AddServerForm


@blueprint.route("/", methods=["GET"])
@login_required
def get_servers():
    """
    Page that displays information about the servers that talk to Hot Potato.
    """

    form = AddServerForm()

    if request.args.get("filter", "") == "missed-hbeat":
        servs = get_missed_hbeats_servs()
    else:
        subquery_last_hbeats = heartbeats.get_last_query().subquery()
        servs = (
            db.session.query(
                Server.hostname,
                Server.timezone,
                Server.enable_heartbeat,
                Server.disable_missed_heartbeat_notif,
                subquery_last_hbeats.c.last_hbeat,
                Server.id,
                Server.link,
            )
            .outerjoin(
                subquery_last_hbeats, subquery_last_hbeats.c.server_id == Server.id
            )
            .all()
        )

    return render_template(
        "list_servers.html",
        now=datetime.utcnow(),
        servs=servs,
        num_missed_hbeats=get_num_missed_hbeats(),
        form=form,
        filter=request.args.get("filter"),
    )


@blueprint.route("/add", methods=["GET", "POST"])
@roles_accepted("admin", hotpotato=True)
def add_server():
    """
    Add a new monitoring server, generating an API key and returning it as json.
    """
    form = AddServerForm()
    if form.validate_on_submit():
        apikey = api_server_key.create()
        servers.create(
            hostname=form.hostname.data,
            apikey=apikey,
            timezone=form.timezone.data,
            link=form.link.data,
            enable_heartbeat=form.enable_heartbeat.data,
            disable_missed_heartbeat_notif=form.disable_missed_heartbeat_notif.data,
        )

        return jsonify({"apikey": apikey})

    return (jsonify(form.errors), 400)


@blueprint.route("/toggle-heartbeat/<server_id>", methods=["POST"])
@roles_accepted("admin", hotpotato=True)
def server_toggle_heartbeat(server_id):
    """
    Toggle whether heartbeats are enabled for a server.
    """

    try:
        server = servers.get(server_id)
    except servers.ServerIDError:
        abort(404)

    server.enable_heartbeat = not server.enable_heartbeat
    db.session.commit()

    return redirect(url_for("views_servers.get_servers"))


@blueprint.route("/toggle-missed-heartbeat-notif/<server_id>", methods=["POST"])
@roles_accepted("admin", hotpotato=True)
def server_toggle_missed_heartbeat(server_id):
    """
    Toggle disabling missed heartbeat notifications for a server.
    """

    try:
        server = servers.get(server_id)
    except servers.ServerIDError:
        abort(404)

    server.disable_missed_heartbeat_notif = not server.disable_missed_heartbeat_notif
    db.session.commit()

    return redirect(url_for("views_servers.get_servers"))
