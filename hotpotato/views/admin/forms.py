import pytz
import wtforms
from flask_security import current_user
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError

from hotpotato import teams, users


class AddTeamForm(FlaskForm):
    name = wtforms.StringField("Team name", validators=[DataRequired()])
    escalates_to = wtforms.SelectField("Escalates to", coerce=int)

    def __init__(self, **kwargs):
        super(AddTeamForm, self).__init__(**kwargs)
        team_list = [(-1, "None")]
        team_list.extend(((team.id, team.name) for team in teams.all()))
        self.escalates_to.choices = team_list

    def validate_name(self, field):
        if teams.get_by(name=self.name.data):
            raise ValidationError("A team with this name already exists.")


class AddUserForm(FlaskForm):
    name = wtforms.StringField("Name", validators=[DataRequired()])
    email = wtforms.StringField("Email", validators=[DataRequired()])
    timezone = wtforms.SelectField(
        "Timezone",
        choices=[(timezone, timezone) for timezone in pytz.all_timezones],
        validators=[DataRequired()],
    )
    primary_team = wtforms.SelectField(
        "Primary Team", validators=[DataRequired()], coerce=int
    )

    def __init__(self, **kwargs):
        super(AddUserForm, self).__init__(**kwargs)
        self.primary_team.choices = [(team.id, team.name) for team in teams.all()]
        if self.timezone.data == "None":
            self.timezone.data = current_user.timezone

    def validate_email(self, field):
        try:
            users.get_by_email(self.email.data)
            raise ValidationError("A user with this email address already exists.")
        except users.UserEmailError:
            pass  # If we get to here the email is not in use and we can use it
