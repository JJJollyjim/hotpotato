from flask import abort, flash, jsonify, redirect, render_template, request, url_for
from passlib.pwd import genphrase

from hotpotato import roles, teams, users
from hotpotato.models import Role, TeamsUsers, User, db, roles_users
from hotpotato.permissions import roles_accepted
from hotpotato.views.admin._blueprint import blueprint
from hotpotato.views.admin.forms import AddTeamForm, AddUserForm

#
# Admin routes
#
# These are only available to admins of *Hot Potato*.


@blueprint.route("/", methods=["GET"])
@roles_accepted("admin", hotpotato=True)
def admin(add_team_form=None):
    """
    Base admin page.
    """

    if add_team_form is None:
        add_team_form = AddTeamForm()
    add_user_form = AddUserForm()

    return render_template(
        "admin/admin.html",
        teams=teams.all(),
        add_team_form=add_team_form,
        add_user_form=add_user_form,
    )


@blueprint.route("/add_team", methods=["GET", "POST"])
@roles_accepted("admin", hotpotato=True)
def add_team():
    form = AddTeamForm()

    if form.validate_on_submit():
        team = teams.create(
            form.name.data,
            escalates_to_id=(
                form.escalates_to.data if form.escalates_to.data != -1 else None
            ),
        )
        return redirect(url_for("views_admin.manage_team", _team_id=team.id))
    return admin(add_team_form=form)


# We can't use 'team_id' here because it conflicts with the global 'current_team' parameter.
@blueprint.route("/manage_team/<_team_id>", methods=["GET", "POST"])
@roles_accepted("admin", hotpotato=True)
def manage_team(_team_id):
    """
    Manage a team
    """
    try:
        team = teams.get(_team_id)
    except teams.TeamIDError:
        abort(404)

    if request.method == "GET":

        return render_template(
            "admin/manage_team.html", team=team, teams=teams.escalatable_teams(_team_id)
        )

    if request.method == "POST":
        f = request.form

        if f["action"] == "escalates_to":
            try:
                team.escalates_to = (
                    teams.get(f["escalates_to"]) if f["escalates_to"] else None
                )
            except teams.TeamIDError:
                flash("The team you are trying to escalate to doesn't exist", "error")
            db.session.commit()

        if f["action"] == "delete":
            if team.teams_users.filter_by(primary=True).count() != 0:
                flash(
                    "You can't delete a team if it's the primary team of any of its users.",
                    "error",
                )
                return redirect(url_for("views_admin.manage_team", _team_id=team.id))
            db.session.delete(team)
            db.session.commit()
            return redirect(url_for("views_admin.admin"))

        if f["action"] == "remove_user":
            try:
                if users.get(f["user"]).primary_team == team:
                    flash("You can't remove a user from their primary team.", "error")
                    return redirect(
                        url_for("views_admin.manage_team", _team_id=team.id)
                    )
            except users.UserIDError:
                flash("User does not exist.", "error")
                return redirect(url_for("views_admin.manage_team", _team_id=team.id))
            team.users.remove(users.get(f["user"]))
            db.session.commit()

        if f["action"] == "set_primary":
            try:
                users.set_primary_team(users.get(f["user"]), team.id)
            except users.UserIDError:
                flash("User does not exist.", "error")
                return redirect(url_for("views_admin.manage_team", _team_id=team.id))
            db.session.commit()

        if f["action"] == "manage_roles":
            # Slightly ugly manipulation here to get the id the roles and if
            # they should be a member.
            roles_ = {k.split("-")[2]: v for k, v in f.items() if k.startswith("role-")}
            try:
                user = users.get(f["user"])
            except users.UserIDError:
                flash("User does not exist.", "error")
                return redirect(url_for("views_admin.manage_team", _team_id=team.id))

            # Remove all roles they were apart of in this team
            # Cockroach doesn't support `DELETE USING` which sqlalchemy
            # generates if we're using a non-subquery delete.
            db.session.query(roles_users).filter(
                roles_users.c.id.in_(
                    db.session.query(roles_users.c.id)
                    .select_from(roles_users.join(User).join(Role))
                    .filter(User.id == user.id, Role.team_id == _team_id)
                )
            ).delete(synchronize_session=False)

            # Set new roles
            users.get(f["user"]).roles.extend(
                [roles.get(role_id) for role_id, member in roles_.items() if member]
            )
            db.session.commit()

        return redirect(url_for("views_admin.manage_team", _team_id=team.id))


@blueprint.route("/manage_team/<_team_id>/add_users", methods=["POST"])
@roles_accepted("admin", hotpotato=True)
def team_add_users(_team_id):
    """
    Add users to a team.
    """
    try:
        team = teams.get(_team_id)
    except teams.TeamIDError:
        abort(404)

    # There may be a nicer way to do this.
    # It's complicated by the association data.
    try:
        _users = [users.get(id) for id in request.form.getlist("users")]
    except users.UserIDError:
        abort(400)
    teams_users = [
        TeamsUsers(primary=False, user=user, team=team)
        for user in _users
        if user not in team.users
    ]
    db.session.add_all(teams_users)
    db.session.commit()

    return redirect(url_for("views_admin.manage_team", _team_id=_team_id))


@blueprint.route("/manage_user/add", methods=["POST"])
@roles_accepted("admin", hotpotato=True)
def new_user():
    """
    Create a new user, generating their password and returning it as json.
    """
    form = AddUserForm()

    if form.validate_on_submit():
        # Generate a password for the user.
        password = genphrase(length=50)

        users.create(
            form.email.data,
            password,
            form.name.data,
            form.timezone.data,
            teams.get(form.primary_team.data),
        )

        return jsonify({"password": password})

    return (jsonify(form.errors), 400)
