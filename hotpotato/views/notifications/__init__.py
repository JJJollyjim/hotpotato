from hotpotato.views.notifications import views
from hotpotato.views.notifications._blueprint import blueprint

__all__ = ["views", "blueprint"]
