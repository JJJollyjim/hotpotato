"""
Notification views blueprint.
"""


import flask

blueprint = flask.Blueprint("views_notifications", __name__)
