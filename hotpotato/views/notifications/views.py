from http import HTTPStatus as Status

from flask import abort, flash, redirect, render_template, request, url_for

from hotpotato.models import db
from hotpotato.notifications import (
    exceptions as notifications_exceptions,
    notifications,
)
from hotpotato.permissions import login_required
from hotpotato.views.notifications import functions
from hotpotato.views.notifications._blueprint import blueprint
from hotpotato.views.notifications.forms import AcknowledgeForm


@blueprint.route("/", methods=["GET"])
@login_required
def notifications_get():
    """
    Display all notifications.
    """

    return functions.render_notifications_page()


@blueprint.route("/<notif_id>", methods=["GET"])
@login_required
def notifications_get_by_id(notif_id):
    """
    Display information about a specific notifications.
    """

    form = AcknowledgeForm()

    try:
        return render_template(
            "notifications/get_by_id.html",
            notif=notifications.get(notif_id, eager=True),
            form=form,
        )

    except notifications_exceptions.NotificationIDError:
        return abort(Status.NOT_FOUND)


@blueprint.route("/acknowledge/<notif_id>", methods=["POST"])
def acknowledge(notif_id):
    try:
        notif = notifications.get(notif_id)
    except notifications.exceptions.NotificationIDError:
        return abort(Status.NOT_FOUND)

    if notif.json["status"] != "SEND_FAILED":
        return abort(Status.BAD_REQUEST)

    notif.json["status"] = "SEND_FAILED_ACKNOWLEDGED"
    db.session.commit()

    flash("Notification acknowledged")

    return redirect(
        request.referrer
        or url_for("views_notifications.notifications_get", **request.args)
    )
