from flask import flash, redirect, url_for
from flask_security import current_user

from hotpotato.models import User, db
from hotpotato.notifications import handovers, messages
from hotpotato.permissions import login_required, roles_accepted
from hotpotato.teams import current_team
from hotpotato.views._blueprint import blueprint
from hotpotato.views.forms import MessageForm
from hotpotato.views.notifications import functions

NULL_USER = User(name="No one")


@blueprint.route("/", methods=["GET"])
@login_required
def main_site():
    """
    The main index of the API.
    """
    return functions.render_notifications_page()


@blueprint.route("/handover", methods=["POST"])
@roles_accepted("admin", "user")
def do_handover():
    """
    Endpoint to do handover.
    """
    form = MessageForm()
    if form.validate_on_submit():
        from_user = current_team.on_call
        to_user = current_user

        # Get the message from the new on-call person.
        message = form.message.data

        # Update on_call for the current team
        current_team.on_call = to_user
        db.session.commit()

        from_user_id = from_user.id if from_user else None

        # Send the handover notifications.
        to_handover, from_handover = handovers.create(
            team_id=current_team.id,
            to_user_id=to_user.id,
            from_user_id=from_user_id,
            message=message,
        )
        to_handover.send()
        if from_handover:
            from_handover.send()

        if from_user:
            flash("You are now on call, taking over from {}.".format(from_user.name))
        else:
            flash("You are now on call.")

    return redirect(url_for("views.main_site"))


@blueprint.route("/message", methods=["POST"])
@login_required
def send_message():
    """
    Endpoint to send a message to the pager person.
    """
    form = MessageForm()
    if form.validate_on_submit():
        body = form.message.data
        from_user = current_user
        to_user = current_team.on_call

        messages.create(
            team_id=current_team.id,
            body=body,
            to_user_id=to_user.id,
            from_user_id=from_user.id,
        ).send()

        if to_user:
            flash("Message sent to {}".format(to_user.name), "success")
        else:
            flash("Message sent", "success")

    return redirect(url_for("views.main_site"))
