"""
Utility variables and functions.
"""


import os
from datetime import datetime

import dateutil
import flask_security
import pytz
from flask import current_app

from hotpotato import notifications

node_name = os.uname().nodename


def enabled_sending_methods():
    """
    Return a list of currently enabled sending methods
    """

    return [
        method
        for method in notifications.METHOD
        if current_app.config["{}_ENABLED".format(method.upper())]
    ]


#
# Conditional functions.
#


def is_mutually_exclusive(*args):
    """
    Return True if:
    * Only one of the given elements are True, and the rest are False
    * All of the given elements are false
    Otherwise (>1 of the given elements are True), return False.
    """

    at_least_one_true = False

    for arg in args:
        if arg:
            if at_least_one_true:
                return False
            else:
                at_least_one_true = True
                # continue
        # else: continue

    return True


#
# Datetime functions.
#


def datetime_get_from_string(timestamp, tz_space_sep=False):
    """
    Convert an ISO 8601-compliant timestamp to a DateTime object.

    The tz_space_sep parameter is used for handling not fully ISO 8601-compliant timestamps
    from older versions of the Hot Potato monitoring system alert handler.
    """

    if tz_space_sep:
        try:
            datetime_dt = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S %z")
        except ValueError:
            datetime_dt = dateutil.parser.isoparse(timestamp)
    else:
        datetime_dt = dateutil.parser.isoparse(timestamp)

    return datetime_dt


def datetime_get_as_string(datetime_obj):
    """
    Return the given DateTime object as an ISO 8601-compliant timestamp.
    """

    return datetime_obj.isoformat(sep=" ")


def datetime_process(
    datetime_dt,
    default_tz=None,
    as_tz=None,
    as_user_tz=False,
    as_utc=False,
    to_tz=None,
    to_user_tz=False,
    to_utc=False,
    naive=False,
):
    """
    Perform transformation operations on the given DateTime object, in the written order.

    If the default_tz parameter is specified, changes the timezone information
    of the DateTime object to default_tz (without modifying the object),
    IF the DateTime object has no timezone already.

    If one of the mutually exclusive parameters as_* parameters are specified,
    changes the timezone information of the DateTime object (without modifying the object).
    * as_tz: changes to the given timezone object
    * as_user_tz: changes to the current Flask user's timezone
    * as_utc: changes to UTC

    If one of the mutually exclusive parameters to_* parameters are specified,
    changes the timezone information AND converts the actual time of the DateTime object.
    NOTE: This requires the DateTime to have a timezone component (either already specified
          in the object, or given to the function via default_tz or as_*) to serve
          as a reference.
    * to_tz: converts to the given timezone object
    * to_user_tz: converts to the current Flask user's timezone
    * to_utc: converts to UTC

    If the naive parameter is specified and the DateTime object has a
    timezone component, this will return a naive DateTime object, while leaving all
    timezone-related processing in place.
    """

    if default_tz and datetime_dt.tzinfo is None:
        datetime_dt = default_tz.localize(datetime_dt)

    if as_tz or as_user_tz or as_utc:
        if not is_mutually_exclusive(as_tz, as_user_tz, as_utc):
            raise ValueError("as_tz, as_user_tz and as_utc are mutually exclusive")
        if as_tz:
            datetime_dt = as_tz.localize(datetime_dt)
        elif as_user_tz:
            datetime_dt = flask_security.current_user.pytz_timezone.localize(
                datetime_dt
            )
        elif as_utc:
            datetime_dt = pytz.utc.localize(datetime_dt)

    if to_tz or to_user_tz or to_utc:
        if not is_mutually_exclusive(to_tz, to_user_tz, to_utc):
            raise ValueError("to_tz, to_user_tz and to_utc are mutually exclusive")
        if datetime_dt.tzinfo is None:
            raise ValueError(
                "datetime_dt needs to be timezone-aware for timezone conversions"
            )
        if to_tz:
            datetime_dt = datetime_dt.astimezone(tz=to_tz)
        elif to_user_tz:
            datetime_dt = datetime_dt.astimezone(
                tz=flask_security.current_user.pytz_timezone
            )
        elif to_utc:
            datetime_dt = datetime_dt.astimezone(tz=pytz.utc)

    if naive and datetime_dt.tzinfo is not None:
        datetime_dt = datetime_dt.replace(tzinfo=None)

    return datetime_dt
