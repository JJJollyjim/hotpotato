"""
User functions.
"""


import flask_security
import sqlalchemy
from sqlalchemy.exc import IntegrityError

from hotpotato.models import TeamsUsers, User as Model, db, user_datastore


class UserError(Exception):
    """
    User exception base class.
    """

    pass


class UserIDError(UserError):
    """
    Exception for invalid user ID.
    """

    def __init__(self, user_id):
        self.user_id = user_id
        super().__init__(
            "Invalid user ID (or unable to find user with ID): {}".format(user_id)
        )


class UserEmailError(UserError):
    """
    Exception for invalid user ID.
    """

    def __init__(self, email):
        self.email = email
        super().__init__(
            "Invalid user email (or unable to find user with email): {}".format(email)
        )


class DuplicateEmailError(UserError):
    pass


class UserTimezoneError(UserError):
    """
    Exception for invalid user timezone.
    """

    def __init__(self, timezone, email=None):
        self.email = email
        self.timezone = timezone
        if self.email:
            super().__init__(
                "Invalid timezone for user with email {}: {}".format(
                    self.email, self.timezone
                )
            )
        else:
            super().__init__("Invalid user timezone: {}".format(self.timezone))


def get(user_id):
    """
    Get a user object using the given ID, and return it.
    """

    try:
        return Model.query.filter_by(id=user_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise UserIDError(user_id)


def get_by_email(email):
    """
    Get a user object using the given email, and return it.
    """

    try:
        return Model.query.filter_by(email=email).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise UserEmailError(email)


def search(name, not_in_team_id=None):
    if not not_in_team_id:
        return (
            Model.query.filter(Model.name.ilike("%{}%".format(name))).limit(100).all()
        )

    return (
        Model.query.filter(
            Model.name.ilike("%{}%".format(name)),
            ~Model.id.in_(
                db.session.query(TeamsUsers.user_id)
                .join(TeamsUsers.team)
                .filter(TeamsUsers.team_id == not_in_team_id)
                .distinct()
            ),
        )
        .limit(100)
        .all()
    )


def get_by(**kwargs):
    """
    Return a list of user objects which match the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def set_primary_team(user, team_id):
    """
    Set a user's primary team.
    """

    # A user must have exactly one primary team, but this is not enforced in SQL so do it manually.
    for teams_users in user.teams_users:
        teams_users.primary = False

    TeamsUsers.query.filter(
        TeamsUsers.user_id == user.id, TeamsUsers.team_id == team_id
    ).first().primary = True

    db.session.commit()


def create(email, password, name, timezone, primary_team, active=True):
    """
    Create a user object using the given input data, add it to the database,
    and return it.
    """

    hashed_password = flask_security.utils.hash_password(password)

    obj = user_datastore.create_user(
        email=email,
        password=hashed_password,
        name=name,
        timezone=timezone,
        active=active,
    )

    join = TeamsUsers(primary=True, team=primary_team)
    obj.teams_users.append(join)

    try:
        db.session.commit()
    except IntegrityError:
        raise DuplicateEmailError()

    return obj
