"""
functions.py contains all the functions used with Hot Potato
"""
from datetime import datetime

import pytz


def to_local_time(date, tz=None, format_="%Y-%m-%d %H:%M:%S"):
    """
    Formats a utc datetime into a user's timezone
    """
    if date.tzinfo is None:
        date = pytz.utc.localize(date)

    if (
        not tz
    ):  # Sometimes an empty string is passed, so this has to be here as an if statement
        tz = "UTC"

    local_date = date.astimezone(pytz.timezone(tz))
    return datetime.strftime(local_date, format_)
