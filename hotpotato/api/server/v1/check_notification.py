"""
Server API version 1 endpoint for checking alerts.
"""


from http import HTTPStatus

import flask
from flask import abort, current_app

from hotpotato import servers
from hotpotato.api.server.v1._blueprint import blueprint
from hotpotato.notifications import alerts, exceptions


# pylint: disable=inconsistent-return-statements
@blueprint.route("/check/notification", methods=["POST"])
def check_notification():
    """
    Allow servers to check the status of an alert that was sent to Hot Potato.
    """

    request = flask.request

    try:
        servers.get_by_api_key(request.form["apikey"])
        alerts.get(request.form["id"])

    except servers.ServerAPIKeyError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.FORBIDDEN)

    except exceptions.NotificationIDError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.NOT_FOUND)

    except exceptions.NotificationTypeError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.UNSUPPORTED_MEDIA_TYPE)

    except KeyError as err:
        current_app.logger.error(err)
        return abort(HTTPStatus.BAD_REQUEST)

    return flask.make_response("true", HTTPStatus.OK)
