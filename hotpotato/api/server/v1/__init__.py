"""
Server API version 1 blueprint and endpoints.
"""


from hotpotato.api.server.v1 import add_notification  # noqa: F401
from hotpotato.api.server.v1 import check_notification  # noqa: F401
from hotpotato.api.server.v1 import heartbeat  # noqa: F401
from hotpotato.api.server.v1 import modica  # noqa: F401
from hotpotato.api.server.v1._blueprint import blueprint  # noqa: F401
