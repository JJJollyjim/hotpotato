"""
Server API version 2 endpoints for Pushover callback functions.
"""


from hotpotato.api.server import pushover
from hotpotato.api.server.v2._blueprint import blueprint


@blueprint.route("/senders/pushover/notificationstatus", methods=["POST"])
def senders_pushover_notificationstatus():
    """
    Handle acknowledgement status receipts from Pushover.
    """

    return pushover.notificationstatus()
