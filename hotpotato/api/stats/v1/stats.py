import datetime

import pytz
from flask import current_app, request
from workalendar.registry import registry as workalendar_registry

from hotpotato.api.stats.v1._blueprint import blueprint
from hotpotato.models import db
from hotpotato.models.notifications.alerts import Alert
from hotpotato.notifications import notifications


def parse_time_range(s):
    """
    Parses a time range into a pair of naïve `time`s.

    >>> parse_time_range("9-17:30") == (datetime.time(9, 0), datetime.time(17, 30))
    """

    start, end = (map(int, x.split(":")) for x in s.split("-"))

    start = datetime.time(*start)
    end = datetime.time(*end)

    return start, end


def query_alert_recieved_times(team_id, ignore_server_ids=[]):
    """
    A generator of recieved_dt values for alerts in the given team.

    They are not all fetched at once, but in fixed-sized chunks.
    """

    # TODO: tweak the limit for optimal performance on real-world data,
    # and confirm there is an appropriate index on received_dt
    limit = 50

    timestamps = None

    while True:
        filters = [Alert.team_id == team_id]

        if timestamps is not None:
            filters.append(Alert.received_dt < timestamps[-1])

        for id_ in ignore_server_ids:
            filters.append(notifications.not_in_json("server_id", id_))

        query = (
            db.session.query(Alert)
            .with_entities(Alert.received_dt)
            .filter(*filters)
            .order_by(Alert.received_dt.desc())
            .limit(limit)
        )

        timestamps = query.all()

        yield from (pytz.utc.localize(ts[0]) for ts in timestamps)

        if len(timestamps) < limit:
            return


def last_matching_alert_datetime(
    team_id, tz, business_hours=None, cal=None, ignore_server_ids=[]
):
    dts = (
        dt.astimezone(tz)
        for dt in query_alert_recieved_times(team_id, ignore_server_ids)
    )

    if cal is None and business_hours is None:
        matching_dts = dts
    else:

        def is_ooh(local):
            if cal is not None:
                if not cal.is_working_day(local.date()):
                    return True

            if business_hours is not None:
                if not (business_hours[0] <= local.time() <= business_hours[1]):
                    return True

            return False

        matching_dts = filter(is_ooh, dts)

    return next(matching_dts)


@blueprint.route("/days_since_alert/<team_id>", methods=["GET"])
def days_since_alert(team_id):
    """
    Return the number of days since this team last had an alert.

    The response will increment at midnight in ``tz``, rather than 24
    hours after the alert.

    Query parametrs:

    tz : required
        For interpretation of ``business_hours`` and ``region``

    business_hours : optional
        Two hours (with optional minutes), seperated by a hyphen (eg
        ``9-17:30``). If provided, only alerts from outside these hours are
        counted towards the response.

        Times are interpreted as being in ``tz``. Hours must be specified as
        24-hour.

    region : optional
        ISO_3166-1 region or ISO_3166-2 subregion, for the purpose of
        determining which days are working days. If provided, only alerts that
        are not on working days are counted towards the response.

        Days are interpreted as being in ``tz``.

    ignore_servers: optional
        comma separated list of ids of servers whose alerts we don't care about


    ``business_hours`` and ``region`` can be combined, so the response
    considers alerts which are on a non-working day AND those which are out of
    business hours.
    """

    try:
        tz = pytz.timezone(request.args.get("tz"))
    except pytz.exceptions.UnknownTimeZoneError:
        return ("Unknown time zone", 400)
    except AttributeError:
        return ("tz is a required parameter", 400)

    business_hours, cal, ignore_server_ids = None, None, []

    if request.args.get("business_hours") is not None:
        try:
            business_hours = parse_time_range(request.args.get("business_hours"))
        except ValueError:
            return ("Invalid business hours specification", 400)

    if request.args.get("region") is not None:
        cal = workalendar_registry.get_calendar_class(request.args.get("region"))()
        if cal is None:
            return ("Unknown region", 400)

    if request.args.get("ignore_servers") is not None:
        try:
            ignore_server_ids = list(
                map(int, request.args.get("ignore_servers").split(","))
            )
        except ValueError:
            return ("Invalid server id", 400)

    # I initially wanted to have this request build up a nice sql query that
    # does all of the OOH checking database-side. Unfortunately, CockroachDB is
    # lacking an important feature from postgres (SET LOCAL timezone = ..., or
    # any other way to perform timezone conversions without altering global
    # connection state), so instead we fetch and iterate through the team's
    # recent alerts in fixed-sized chunks until we find one that meets the
    # criteria

    try:
        dt = last_matching_alert_datetime(
            team_id, tz, business_hours, cal, ignore_server_ids
        )
    except StopIteration:
        # There has never been an alert matching the criteria
        return "", 204

    current_app.logger.info(dt)
    days = (datetime.datetime.now(tz=tz).date() - dt.date()).days

    return str(days), 200, {"Content-Type": "text/plain; charset=utf-8"}
