"""
Web API version 1 endpoints for getting notifications.
"""


from http import HTTPStatus

from hotpotato.api import functions as api_functions
from hotpotato.api.functions import notifications as api_functions_notifications
from hotpotato.api.web.v1 import (
    decorators as api_web_decorators,
    functions as api_web_functions,
)
from hotpotato.api.web.v1._blueprint import blueprint
from hotpotato.api.web.v1.notifications import handlers as notifications_handlers
from hotpotato.notifications import exceptions as notifications_exceptions


@blueprint.route("/notifications/get", methods=["GET", "POST"])
@api_web_decorators.login_required
def notifications_get():
    """
    Return a dictionary of notifications, found using the given search parameters.
    """

    return _notifications_get()


@blueprint.route("/notifications/<handler_name>/get", methods=["GET", "POST"])
@api_web_decorators.login_required
def notifications_get_for_type(handler_name):
    """
    Return a dictionary of notifications of a given type, found using
    the given search parameters.
    """

    return _notifications_get(handler_name=handler_name)


@blueprint.route("/notifications/get/as_csv", methods=["GET", "POST"])
@api_web_decorators.login_required
def notifications_get_as_csv():
    """
    Return a CSV document of notifications, found using the given search parameters.
    """

    return _notifications_get_as_csv()


@blueprint.route("/notifications/<handler_name>/get/as_csv", methods=["GET", "POST"])
@api_web_decorators.login_required
def notifications_get_for_type_as_csv(handler_name):
    """
    Return a CSV document of notifications of a given type, found using
    the given search parameters.
    """

    return _notifications_get_as_csv(handler_name=handler_name)


@blueprint.route("/notifications/get/<notif_id>", methods=["GET"])
@api_web_decorators.login_required
def notifications_get_by_id(notif_id):
    """
    Return the notification with the given ID in a JSON object.
    """

    return _notifications_get_by_id(notif_id)


@blueprint.route("/notifications/get/<notif_id>/as_csv", methods=["GET"])
@api_web_decorators.login_required
def notifications_get_by_id_as_csv(notif_id):
    """
    Return the notification with the given ID in a CSV document.
    """

    return _notifications_get_by_id_as_csv(notif_id)


@blueprint.route("/notifications/<handler_name>/get/<notif_id>", methods=["GET"])
@api_web_decorators.login_required
def notifications_get_for_type_by_id(handler_name, notif_id):
    """
    Return the notification of the given type, with the given ID in a JSON object.
    """

    return _notifications_get_by_id(notif_id, handler_name=handler_name)


@blueprint.route("/notifications/<handler_name>/get/<notif_id>/as_csv", methods=["GET"])
@api_web_decorators.login_required
def notifications_get_for_type_by_id_as_csv(handler_name, notif_id):
    """
    Return the notification of the given type, with the given ID in a CSV document.
    """

    return _notifications_get_by_id_as_csv(notif_id, handler_name=handler_name)


#
# API endpoint subroutines.
#


def _notifications_get(handler_name="notifications"):
    """
    Return a dictionary of notifications of a given type, found using
    the given search parameters.
    """

    handler = notifications_handlers.get(handler_name)
    search_filters = api_functions_notifications.search_filters_get(handler=handler)
    notifs = api_functions_notifications.get_from_search_filters(
        handler, search_filters
    )

    return api_web_functions.api_json_response_get(
        data={
            notif.id: api_functions_notifications.as_dict(handler, notif)
            for notif in notifs
        }
    )


def _notifications_get_as_csv(handler_name="notifications"):
    """
    Return a list of notifications in CSV format.
    """

    handler = notifications_handlers.get(handler_name)
    search_filters = api_functions_notifications.search_filters_get(handler=handler)
    notifs = api_functions_notifications.get_from_search_filters(
        handler, search_filters
    )

    return api_functions.csv_response_get(
        *api_functions_notifications.as_csv(handler, notifs)
    )


def _notifications_get_by_id(notif_id, handler_name="notifications"):
    """
    Get a notification using the given ID.
    """

    try:
        handler = notifications_handlers.get(handler_name)
        notif = api_functions_notifications.get(handler, notif_id)

        return api_web_functions.api_json_response_get(
            data=api_functions_notifications.as_dict(handler, notif)
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )


def _notifications_get_by_id_as_csv(notif_id, handler_name="notifications"):
    """
    Get a notification using the given ID, and return it in CSV format.
    """

    try:
        handler = notifications_handlers.get(handler_name)
        notifs = (api_functions_notifications.get(handler, notif_id),)

        return api_functions.csv_response_get(
            *api_functions_notifications.as_csv(handler, notifs)
        )

    except notifications_exceptions.NotificationIDError as err:
        return api_web_functions.json_response_get(
            success=False, code=HTTPStatus.NOT_FOUND, message=str(err)
        )
