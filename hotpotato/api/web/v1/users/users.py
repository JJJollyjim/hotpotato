from flask import jsonify, request

from hotpotato.api.web.v1._blueprint import blueprint
from hotpotato.api.web.v1.decorators import login_required
from hotpotato.users import search


@blueprint.route("/users/get", methods=["GET"])
@login_required
def users_get():
    users = search(
        request.args.get("query"), not_in_team_id=request.args.get("__team_id")
    )
    return jsonify(
        [
            {"id": str(user.id), "name": user.name, "last_login_at": user.last_login_at}
            for user in users
        ]
    )
