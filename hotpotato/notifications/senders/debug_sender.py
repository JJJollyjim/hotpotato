"""
A debug sender which logs all messages sent
"""

import logging

import flask

from hotpotato.notifications import PROVIDER
from hotpotato.notifications.senders.base import BaseSender

logger = logging.getLogger(__name__)


class DebugSender(BaseSender):
    """
    Test messsage sender
    """

    name = "debug_sender"
    description = "debug_sender"

    # Lie about the provider to get past message integrity checks
    provider = next(PROVIDER.__iter__())

    def __init__(self, method):
        """
        Pretend to be whatever provider was supposed to send the message
        """
        self.method = method

    def send(self, notif, contact):
        """
        Log the message
        """

        message = {
            "type": notif.notif_type,
            "user": contact.contact,
            "message": notif.body,
            "timestamp": notif.received_dt.timestamp(),
            "url": flask.url_for(
                "api_web_v1.notifications_get_by_id", notif_id=notif.id
            ),
        }

        logger.info("(VIA {}) DEBUG SENDER MESSAGE: {}".format(self.method, message))

        return {
            "success": True,
            "warnings": [],
            "errors": [],
            "provider_notif_id": None,
        }
