"""
Message classes and functions.
"""


from hotpotato import users
from hotpotato.models import Message
from hotpotato.notifications import notifications

NOTIF_TYPE = "message"

# A frozen set of values representing all possible JSON API parameters for a message.
JSON_API_PARAMS = frozenset(("from_user_id", "reply_to_notif_id"))


def get(mess_id):
    """
    Get a message.
    """

    return notifications.get(mess_id)


def get_by(**kwargs):
    """
    Return a tuple of all messages matching the given search parameters.
    """

    kwargs["notif_type"] = "message"
    return notifications.get_by_helper(**kwargs)


def create(team_id, body, to_user_id=None, from_user_id=None, reply_to_notif_id=None):
    """
    Create a new message.
    """

    return notifications.create(
        team_id=team_id,
        notif_model=Message,
        body="Message from {} via Hot Potato: {}".format(
            users.get(from_user_id).name, body
        )
        if from_user_id
        else "Message from Hot Potato: {}".format(body),
        user_id=to_user_id,
        json_obj={
            "from_user_id": from_user_id,
            "reply_to_notif_id": reply_to_notif_id,
            "message": body,
        },
    )
