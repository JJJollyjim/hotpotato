"""
Flask app helper functions.
"""


import pprint
import warnings  # Apply warning suppression.

# Normal package imports.
import flask  # noqa: E402
import flask_mail  # noqa: E402

from hotpotato import __version__  # noqa: E402
from hotpotato import api  # noqa: E402
from hotpotato import cli  # noqa: E402
from hotpotato import config  # noqa: E402
from hotpotato import logging  # noqa: E402
from hotpotato import models  # noqa: E402
from hotpotato import util  # noqa: E402
from hotpotato import views  # noqa: E402
from hotpotato import teams
from hotpotato.notifications import queue as notifications_queue  # noqa: E402

from hotpotato.patches import (  # noqa: E402, F401; Apply monkey patches.
    flask_security as patches_flask_security,
)

# Suppress psycopg2 2.7.4 UserWarning relating to the binary package
# being separated into its own package.
# TODO: Remove this when 2.8 is released, and remove the following pylint warning suppression.
# and remove E402 suppression
warnings.filterwarnings("ignore", module="psycopg2", category=UserWarning)


def create(
    init_cli=True,
    init_config=True,
    config_preset=None,
    config_filepaths=None,
    config_use_env=True,
    config_use_default_paths=True,
    init_logging=True,
    init_models=True,
    init_notifications_queue=True,
    init_views=True,
    init_api=True,
    setup_server_name=False,
):
    """
    Create and return a Flask app.
    """

    app = flask.Flask(__name__)

    if init_cli:
        cli.init_app(app)

    if init_config:
        config.init_app(
            app,
            preset=config_preset,
            filepaths=config_filepaths,
            use_env=config_use_env,
            use_default_paths=config_use_default_paths,
            setup_server_name=setup_server_name,
        )
    if init_logging:
        logging.init_app(app)

    if init_models:
        models.init_app(app)

    if init_notifications_queue:
        notifications_queue.init_app(app)

    if init_views:
        views.init_app(app)

    if init_api:
        api.init_app(app)

    if app.debug:
        import flask_debugtoolbar

        # This is annoying, messes with tests, and generally useless
        app.config["DEBUG_TB_INTERCEPT_REDIRECTS"] = False

        flask_debugtoolbar.DebugToolbarExtension().init_app(app)
        views.csrf.exempt(flask_debugtoolbar.module)

    teams.init_app(app)

    with app.app_context():
        flask.g.mail = flask_mail.Mail(app)
        flask.g.server_name = util.node_name
        flask.g.version = "Version {}".format(__version__)

    with app.app_context():
        if app.config["TESTING"]:
            app.logger.debug("")
            app.logger.debug("Hot Potato")
            app.logger.debug("Version: {}".format(__version__))
            app.logger.debug("")
            app.logger.debug(
                "Configuration:\n{}".format(pprint.pformat(app.config, indent=2))
            )
            app.logger.debug("")

    return app
