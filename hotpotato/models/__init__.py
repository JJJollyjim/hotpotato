"""
Model public fields, classes and helper methods.
"""


import os
import pathlib

import alembic
import flask
import flask_migrate
import flask_security
import sqlalchemy.ext.mutable
import sqlalchemy_utils

import hotpotato
from hotpotato.models.database import db
from hotpotato.models.heartbeats import Heartbeat
from hotpotato.models.notifications.alerts import Alert
from hotpotato.models.notifications.handovers import Handover
from hotpotato.models.notifications.messages import Message
from hotpotato.models.notifications.notifications import Notification
from hotpotato.models.oncall_contacts import OncallContact
from hotpotato.models.server_uptimes import ServerUptime
from hotpotato.models.servers import Server
from hotpotato.models.teams import Team, TeamsUsers
from hotpotato.models.users import Role, User, roles_users, user_datastore

__all__ = [
    "Heartbeat",
    "Alert",
    "Handover",
    "Message",
    "Notification",
    "OncallContact",
    "ServerUptime",
    "Server",
    "Role",
    "User",
    "user_datastore",
    "init_app",
    "create",
    "initialise",
    "reinitialise",
    "Team",
    "TeamsUsers",
    "roles_users",
]

migrate = flask_migrate.Migrate()


#
# Helper methods.
#


def init_app(app):
    """
    Initialise the database models and the migration system for the given app.
    """

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    directory = str(
        pathlib.Path(os.path.dirname(os.path.abspath(__file__))).parent / "migrations"
    )
    migrate.init_app(app, db, directory=directory)
    with app.app_context():
        flask.g.security = flask_security.Security(app, user_datastore)


def create(app):
    """
    Create the database for the given app, if it doesn't exist.
    """

    url = sqlalchemy.engine.url.make_url(app.config["SQLALCHEMY_DATABASE_URI"])

    if sqlalchemy_utils.database_exists(url):
        return

    engine = sqlalchemy.create_engine(
        "cockroachdb://{}:{}/system".format(url.host, url.port)
    )
    conn = engine.connect()
    conn.execute("CREATE DATABASE {}".format(url.database))
    conn.close()

    if not sqlalchemy_utils.database_exists(url):
        raise RuntimeError("Unable to create database: {}".format(url))


def initialise(app):
    """
    Initialise the database with all models and populate it with initial data.
    """

    with app.app_context():
        db.create_all()

        # Stamp the database with the current alembic migration version
        cfg_path = str(
            pathlib.Path(os.path.dirname(os.path.abspath(__file__))).parent
            / "migrations"
            / "alembic.ini"
        )
        alembic_cfg = alembic.config.Config(cfg_path)
        alembic_cfg.set_main_option("script_location", "hotpotato:migrations")
        alembic.command.stamp(alembic_cfg, "head")

        # hotpotato.teams CANNOT be imported here because it's cyclic
        # However, importing hotpotato works, because by the time it's
        # evaluated both modules have been loaded
        try:
            team = hotpotato.teams.hotpotato_team()
        except sqlalchemy.orm.exc.NoResultFound:
            team = hotpotato.teams.create(hotpotato.teams.HOTPOTATO_TEAM_NAME)

        for name, description in hotpotato.roles.DEFAULT.items():
            user_datastore.find_or_create_role(
                name=name, description=description, team=team
            )
        db.session.commit()


def reinitialise(app):
    """
    Reinitialise a database, by dropping all tables and repopulating it
    with initial data.
    """

    with app.app_context():
        db.drop_all()
    initialise(app)
