"""
Team models
"""


from hotpotato.models.database import db


class TeamsUsers(db.Model):
    user_id = db.Column(
        db.Integer(), db.ForeignKey("user.id", ondelete="CASCADE"), primary_key=True
    )
    user = db.relationship("User", back_populates="teams_users")
    team_id = db.Column(
        db.Integer(), db.ForeignKey("team.id", ondelete="CASCADE"), primary_key=True
    )
    team = db.relationship("Team", back_populates="teams_users")
    primary = db.Column(db.Boolean(), nullable=False)


class Team(db.Model):
    """
    A team is a group within an organisation responsible for a particular
    service or group of services. One project might first be monitored by
    the dev team, then be monitored by an operations team when it reaches
    production.

    Notifications are initially sent to a single team. However, if they are
    not acknowledged or not in business hours for the team they can be
    escalated to another team.

    Teams that are escalated to have all permissions of the teams below them.
    """

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)
    # The team that notifications can be escalated to from this team,
    escalates_to_id = db.Column(
        db.Integer, db.ForeignKey("team.id", ondelete="RESTRICT")
    )
    escalates_to = db.relationship("Team", remote_side=[id], backref="escalated_from")

    on_call_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    on_call = db.relationship("User")

    teams_users = db.relationship(
        "TeamsUsers", back_populates="team", cascade="delete", lazy="dynamic"
    )

    __table_args__ = (
        db.CheckConstraint("id != escalates_to_id", "not_escalates_to_self"),
    )
