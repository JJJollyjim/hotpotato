"""
Flask SQLAlchemy setup.
"""


import flask_sqlalchemy


# pool_pre_ping is enabled on the underlying SQLAlchemy engine to
# make sure connections are actually active before Hot Potato tries to
# use them in a request.
class HotPotatoSQLAlchemy(flask_sqlalchemy.SQLAlchemy):
    def apply_pool_defaults(self, app, options):
        options["pool_pre_ping"] = True
        super().apply_pool_defaults(app, options)


db = HotPotatoSQLAlchemy()
