"""
User model classes.
"""
import flask_security
import pytz
from sqlalchemy.orm import validates

import hotpotato
from hotpotato.models.database import db

# Maps users to roles.
roles_users = db.Table(
    "roles_users",
    db.Column("id", db.Integer(), primary_key=True),
    db.Column("user_id", db.Integer(), db.ForeignKey("user.id", ondelete="CASCADE")),
    db.Column("role_id", db.Integer(), db.ForeignKey("role.id", ondelete="CASCADE")),
)


class Role(db.Model, flask_security.RoleMixin):
    """
    Stores the various user roles.
    """

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255))
    description = db.Column(db.Text)

    team_id = db.Column(
        db.Integer, db.ForeignKey("team.id", ondelete="CASCADE"), nullable=False
    )
    team = db.relationship(
        "Team",
        backref=db.backref("roles", lazy="dynamic", cascade="all, delete-orphan"),
    )

    __table_args__ = (db.UniqueConstraint("name", "team_id"),)

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class User(db.Model, flask_security.UserMixin):
    """
    Stores user information.
    """

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.Text)
    name = db.Column(db.Text)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.Text)
    current_login_ip = db.Column(db.Text)
    login_count = db.Column(db.Integer())
    timezone = db.Column(db.Text)
    roles = db.relationship(
        "Role", secondary=roles_users, backref=db.backref("users", lazy="dynamic")
    )

    teams = db.relationship(
        "Team",
        secondary="teams_users",
        backref=db.backref("users", lazy="dynamic"),
        lazy="dynamic",
    )
    teams_users = db.relationship("TeamsUsers", back_populates="user")

    _primary_team = None

    # TODO: Replace this with a relationship
    # Requires: https://github.com/cockroachdb/cockroach/issues/3288
    @property
    def primary_team(self):
        """
        Get this user's primary team.
        """

        if not self._primary_team:
            self._primary_team = self.teams.filter(
                hotpotato.models.teams.TeamsUsers.primary == True  # noqa: E712
            ).one_or_none()

        return self._primary_team

    @property
    def pytz_timezone(self):
        """
        Return the timezone of the user as a timezone object.
        """

        try:
            return pytz.timezone(self.timezone or "UTC")
        except pytz.UnknownTimeZoneError:
            return pytz.timezone("UTC")

    @validates("timezone")
    def validate_timezone(self, key, timezone):
        """
        Check timezone is a valid pytz timezone.
        """

        if timezone not in pytz.all_timezones:
            from hotpotato.users import UserTimezoneError

            raise UserTimezoneError(timezone, self.email)

        return timezone

    def add_role(self, role):
        """
        Add a role to this user.
        """

        user_datastore.add_role_to_user(self, role)


# Initialize the Flask-Security user datastore.
user_datastore = flask_security.SQLAlchemyUserDatastore(db, User, Role)
