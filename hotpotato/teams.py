"""
Team related functions.
Warning: contains magic.
"""


from functools import wraps
from urllib.parse import urlencode

import sqlalchemy
from flask import abort, g, redirect, request
from flask_security import current_user
from sqlalchemy.exc import IntegrityError
from werkzeug.local import LocalProxy

from hotpotato import roles
from hotpotato.models import Team as Model, db

HOTPOTATO_TEAM_NAME = "Hot Potato"


class TeamError(Exception):
    pass


class DuplicateTeamName(TeamError):
    pass


class TeamIDError(TeamError):
    pass


def hotpotato_team():
    return Model.query.filter(Model.name == HOTPOTATO_TEAM_NAME).one()


def get(team_id):

    try:
        return Model.query.filter_by(id=team_id).one()
    except (sqlalchemy.orm.exc.NoResultFound, sqlalchemy.exc.DataError):
        raise TeamIDError()


def get_by(**kwargs):
    """
    Return a tuple containing all team objects matching the given search parameters.
    """

    return Model.query.filter_by(**kwargs).all()


def all():
    return Model.query.all()


def _get_team():
    """
    Try to get the current team from the request url, redirect if it's not
    found and there is a user logged in.
    """

    # TODO: Should we do permissions checks here?

    if "current_team" not in g:
        team_id = request.args.get("team_id")

        # Redirect if team_id not found
        if not team_id:
            if (
                current_user.is_anonymous
                or request.method != "GET"
                or request.path.startswith("/api")
            ):
                return None

            new = request.args.copy()
            new["team_id"] = current_user.primary_team.id
            abort(redirect("{}?{}".format(request.path, urlencode(new))))

        try:
            g.current_team = get(team_id)
        except TeamIDError:
            abort(404)

    return g.current_team


def team_injector(route, args):
    """
    This inserts the team_id into the route parameters if it is not already set.
    If the route takes team_id as a paramater it will be set to this value, otherwise
    it will be passed as a query parameter.
    """

    # flask-debugtoolbar doesn't use url_for correctly, so ignore it's routes
    if "_debug_toolbar" in route:
        return

    try:
        args["team_id"] = current_team.id
    # In order: no team_id, no current_team, not in request context (in tests)
    except (KeyError, AttributeError, RuntimeError):
        pass


def team_decorator(fn):
    """
    Inject the team_id into the keyword arguments of the decorated function,
    if it is not already there.

    The resulting decorated view also has the `inner_fn` attribute,
    which gives access to the inner function definition, just in case the
    caller needs it. This is specifically used for testing at the moment.
    """

    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if "team_id" not in kwargs:
            kwargs["team_id"] = current_team
        return fn(*args, **kwargs)

    decorated_view.inner_fn = fn

    return decorated_view


"""
Proxy for the current team.
This is determined from the `team_id` query parameter which is injected into
every route by `team_injector`. If `team_id` is not found we redirect to the
same url with with the current user's primary team id added.
"""
current_team = LocalProxy(_get_team)


def init_app(app):
    """
    Sets up url_for rewriting. See team_injector docstring.
    """

    app.url_defaults(team_injector)


def escalatable_teams(team_id):
    """
    Return teams that `team` is allowed to escalate to.

    TODO: Detect cycles here?
    """

    return Model.query.filter(Model.id != team_id).all()


def create(name, escalates_to_id=None, on_call_id=None):
    """
    Create a new team, optionally setting escalations and on call person.
    """

    try:
        team = Model(name=name, escalates_to_id=escalates_to_id, on_call_id=on_call_id)
        db.session.add(team)
        db.session.commit()
    except IntegrityError:
        raise DuplicateTeamName()

    # Add default roles
    for name, description in roles.DEFAULT.items():
        team.roles.append(roles.create(team.id, name, description))

    db.session.commit()

    return team
